﻿module ExerciseSet1


(* Exercise 1.1 *)
let sqr x = 
    x * x

(* Exercise 1.2 *)
let pow x n = 
    System.Math.Pow(x, n)

(* Exercise 1.3 *)
let g = (+) 4

(* Exercise 1.4 *)
let h x y = 
    System.Math.Sqrt((x*x) + (y*y))


(* Exercise 1.5 *)
let rec f n =
    match n with
    | 0 -> n    
    | n -> n + f(n-1)

(* Exercise 1.6 *)

let rec F n =
    match n with
    | 0 -> 0
    | 1 -> 1
    | n -> n + F(n-1)

// F 4 evaluates to 10


(* Exercise 1.7 *)

let rec sum (m, n) =
    match (m,n) with
    | (m, 0) -> m
    | (m, n) -> (m+n)+ sum(m, (n-1))


(* Exercise 1.8 *)
(*

Expression 1:
(System.Math.PI, fact -1)
Has the type:
float * int

Expression 2:
fact(fact 4)

Has the type:
int -> int

Expression 3:
power(System.Math.PI, fact 2)

Has the type:
float * int -> float

Expression 4:
(power, fact)

Has the type
float * int

*)

(* Exerise 1.9 *)

(*
The environment obtained would be:
[ a     -> 5        ]
[ f     -> a + 1    ]
[ g     -> (f b) + a]

f 3 evalutes to 4

g 3 evalutes to 9

*)




(* Exercise 1.10 *)
let dup (s:string) = 
    s + s


(* Exercise 1.11 *)
let rec dupn (s:string) n = if n > 1 then s + dupn s (n-1) else s


(* Exercise 1.12 *)
let timediff t1 t2 =
    match t1 with
    | (a, b) -> 
        match t2 with
        | (c, d) -> ((c-a) * 60) + d-b


(* Exercise 1.13 *)
let minutes (t: int * int) =
    timediff (00,00) t 


(* Exercise 1.14 *)
let rec pow114 (s: string * int) =
    match s with
    | (a, n) -> if n > 1 then a + pow114 (a, n-1) else a 


(* Exercise 1.15 *)
let rec bin (n, k) = 
    match (n, k) with
    | (n, 0) -> 1
    | (n, k) when n = k ->  1
    | (n, k) when k >= n -> failwith "k >= n"
    | (n, k) when n = 0 || k = 0 -> failwith "n or k is equal to 0"
    | (n, k) ->  bin(n-1, k-1) + bin(n-1, k)


(* Exercise 1.16 *)
let rec fu = function
    | (0,y) -> y
    | (x,y) -> fu(x-1, x*y)

//F has the type int * int -> int

(* Exercise 1.17 *)
let test (c,e)=
    if c then e else 0

// Used to test, taken from the course book: Functional Programming Using F#, page 8
let rec fact = function
    | 0 -> 1
    | n -> n * fact(n-1)
(*
1. Test has the type bool * int -> int
2. Evaluating test(false, fact(-1)) gives gives stack overflow
3. if false then fact -1 else 0 evalutes to 0. 
    It make sense the statement in 3. evalutes to 0, as when you type in the statement it never evalutes "fact -1", but simply
    evalutes 0, where in the function in 2, both alternatives are evalutated and as fact -1 gives stack overflow exception, 
    the function returns stack overflow exception.
    The reason why fact -1 is evaluted is due to the F# compiler, at compile time, calculates and puts into our environment all calculateable variables. (
    As any other compiler would do)


*)


(* Exercise 1.18 *)

let curry f = fun g -> fun h -> f (g, h)

let uncurry g = fun (x, y) -> g x y