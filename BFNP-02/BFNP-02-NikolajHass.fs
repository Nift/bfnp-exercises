﻿module Exercise2

(* Exercise 2.1 *)
let rec downTo n = 
    if n > 0 then [n]::downTo (n-1) else []

let rec downTo2 n =
    match n with 
    | 0 -> []
    | n -> [n]::downTo2(n-1)


(* Exercise 2.2 *)

let rec removeOddIdx xs  =
    match xs with
    | [] -> []
    | x::y::xs -> x::removeOddIdx(xs) 
    | x::xs -> [x]


(* Exercise 2.3 *)

let rec combinePair xs =
    match xs with
    | [] -> []
    | x::y::xs -> (x, y)::combinePair(xs)
    | _ -> []

(* Exercise 2.4 *)

type cur= {pounds:int; shillings:int; pence:int }

let CurToPence c =  ((c.pounds * 20) * 12) + (c.shillings * 12) + c.pence

let PenceToCur c = 
    let newC = {pounds = 0; shillings = c.pence / 12; pence = c.pence % 12}
    {pounds = newC.shillings/20; shillings = newC.shillings % 20; pence = newC.pence}

let (.+) c1 c2 =  
    match (c1, c2) with
    | (c1, c2) when CurToPence c1 > -1 && CurToPence c2 > -1 -> 
        PenceToCur {pounds = 0; shillings = 0; pence = (CurToPence c1 + CurToPence c2)}
    | (_,_) -> failwith "Not a currency"
    

let (.-) c1 c2 =
    match (c1, c2) with
    |(c1, c2) when (CurToPence c1 - CurToPence c2) > 0 -> 
        PenceToCur {pounds = 0; shillings = 0; pence = (CurToPence c1 - CurToPence c2)}
    |(c1, c2) when (CurToPence c2 - CurToPence c1) > 0 -> 
        PenceToCur {pounds = 0; shillings = 0; pence = (CurToPence c2 - CurToPence c1)}
    |(_,_) -> failwith "Not a currency"    
     


(* Exercise 2.5 *)

// 1. 
let (.+.) (a,b) (c, d) = (a+c, b+d)


let (.*.) (a, b) (c,d) = (a*c - b*d, b*c + a*d)

// 2.
let (-.) (a,b) (c,d) = (a, b) .+. (-c, -d)

let (./.) (a,b) (c,d) =  (a,b) .*. ((c/(c*c + d*d)), (-d/(c*c + d*d)))

// 3. 

let (.//.) (a, b) (c,d) =
    let tmp = c*c + d*d
    (a,b) .*.  (c/tmp, -d/tmp)

(* Exercise 2.6 *)

let rec altsum = function
| [] -> 0
| x::xs -> x - altsum xs

(* Exercise 2.7 *)

let explode (s:string) = List.ofArray(s.ToCharArray())

let rec explode2 (s:string) = 
    match s with
    | s when s.Length < 1 -> []
    | s -> s.[0]::explode2(s.Remove(0,1))
    | _ -> []



(* Exercise 2.8 *)

let implode (xs : char list) = 
    List.foldBack (fun  x y ->  string x + y ) xs ""
    
let implodeRev  (xs : char list) =
    List.fold (fun x y ->  string y + x) "" xs

(* Exercise 2.9 *)

let toUpper s =  implode ( List.map (fun x -> System.Char.ToUpper x) (explode s) )

let toUpper2 = ((fun s -> List.map (fun x -> System.Char.ToUpper x) (explode s)) >> (fun s -> implode s))

let toUpper3 = ((fun s -> implode s) << (fun s -> explode s |> List.map( fun x -> System.Char.ToUpper x)))


(* Exercise 2.10 *)

let palindrome (s:string) = System.String.Equals(s, implodeRev(explode(s)), System.StringComparison.CurrentCultureIgnoreCase)

(* Exercise 2.11 *)

let rec ack p =
    match p with
    | (m, n) when m = 0 -> n + 1
    | (m, n) when m > 0 && n = 0 -> ack(m-1, 1)
    | (m, n) when m > 0 && n > 0-> ack(m-1, (ack(m, n-1)))
    | (m, n) when m < 0 || n < 0 -> failwith "Ack only takes non-negative numbers!"
    | _ -> failwith "Not an int tuple!"
    
(* Exercise 2.12  *)

let time f =
    let start = System.DateTime.Now in
    let res = f () in
    let finish = System.DateTime.Now in
    (res, finish - start)

let timeArg1 f a = time (fun () -> f a)

(* Exercise 2.13 *)

let rec downto1 f n e =
    match n with
    | n when n > 0 -> downto1 f (n-1) (f n e)
    | n when n <= 0 -> e
    | _ -> failwith "Wrong parameters given"
    
let fact n = downto1 (fun x y -> x*y) n 1

let buildList g n = downto1 (fun x xs -> g(x)::xs) n []


