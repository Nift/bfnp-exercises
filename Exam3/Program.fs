﻿type item = { id : int; 
           name : string;
           price : float}

type register = item list

//Question 1.1
let regis = [{id = 1; name = "Milk"; price = 8.75};
             {id = 2; name = "Juice"; price = 16.25};
             {id = 3; name = "Rye Bread"; price = 25.00};
             {id = 4; name = "White Bread"; price = 18.50}]

// Question 1.2
exception Register of string

let getItemById id (reg:register) =
    match List.tryFind (fun f -> id = f.id) reg with
    | Some x -> x
    | None -> raise (Register "Not a valid ID")


//Question 1.3
(*Unsure of this?*)
let nextId (reg:register) = 
    let rec f (re:register) l =
        match reg with
        | x::xs -> if x.id > l then f xs x.id else f xs l
        | [] -> l
   
    (f reg 0)+1

//Question 1.4

let addItem n p r : register = 
    r@[{id = nextId r; name = n; price = p}]


//Question 1.5
let rec deleteItemById id (reg:register) : register = 
    match reg with
    | x::xs -> if x.id = id then xs else deleteItemById id reg
    | [] -> reg


//Question 1.6
let rec uniqueRegister (reg:register)  = 
    match reg with
    | x::xs -> if List.exists (fun f -> f.id = x.id) xs then false else uniqueRegister xs
    | [] -> true


//Question 1.7
let itemsInPriceRange p d (r:register) : register =
    let rec getProducts low high (r:register) (finalList:register) =
        match r with
        | x::xs -> if x.price > low && x.price < high then getProducts low high xs (x::finalList) else getProducts low high xs finalList
        | [] -> finalList
    
    getProducts (p-d) (p+d) r [] 



//QUestion 2.1
//Functions type is int -> int -> int
//The function computes the calculation of the mutplication of n, m times, where n is incremented by one for each iteration. 


let rec f n m = 
    if m = 0 then n else n * f (n+1) (m-1)

//Question 2.2
let rec f' n m acc = 
    if m = 0 then (acc*n) else f' (n+1) (m-1) (n*acc)

// QUestion 2.3
let rec z xs  ys = 
    match (xs, ys) with
    | ([],[]) -> []
    | (x::xs, []) -> (x,x) :: (z xs ys)
    | ([], y::ys) -> (y,y) :: (z xs ys)
    | (x::xs, y::ys) -> (x,y)::(z xs ys)


//Question 2.4
let rec s xs ys =
    match (xs, ys) with
    | ([], []) -> []
    | (xs, []) -> xs
    | ([], ys) -> ys
    | (x::xs, y::ys) -> x::y::s xs ys

//Question 2.5

let rec sC xs ys a = 
    match (xs, ys) with
    | ([], []) -> a
    | (xs, []) -> a@xs
    | ([], ys) -> a@ys
    | (x::xs, y::ys) -> sC xs ys (x::y::a)


//Question 3

type Latex<'a> =
    Section of string * 'a * Latex<'a>
    | Subsection of string * 'a * Latex<'a>
    | Label of string * Latex<'a>
    | Text of string * Latex<'a>
    | Ref of string * Latex<'a>
    | End

let text1 = Section ("Introduction", None, Text ("This is an introduction to...", Subsection("A subsection", None, Text("As laid out in the introduction we ...", End))))

let text2 = Section("Introduction", None, Text("This is an introduction to ...", Subsection("A subsection", None, Text("As laid out in the introduction we ...", Subsection("Yet a subsection", None, Section("And yet a section", None, Subsection("A subsection more ...", None, End)))))))


//Question 3.1
//The type is Latex<'a option>


//Question 3.2
let addSecNumbers (s : Latex<'a>) =
        let rec iterate (p : Latex<'a>) (start:int) (substart:int) =
            match p with
            | Section(s, a, t) -> Section(s, start.ToString(), (iterate t (start+1) 1)) 
            | Subsection(s, a , t) -> Subsection(s, ( (start-1).ToString() + "." + substart.ToString()), iterate t start (substart+1)) 
            | Text(s, t)  -> Text(s, (iterate t start substart))
            | Label(s,t) -> Label(s, (iterate t start substart))
            | Ref(s,t) -> Ref(s,(iterate t start substart))
            | End -> End
        iterate s 1 1


//Question 3.3
//Latex<'a> -> Latex<string>

let text3 = Section("Introduction", "1", Label("intro.sec", Text("In section", Ref("subsec.sec", Text(" we describe..", Subsection("A subsection", "1.1", Label("subsec.sec", Text("As laid out in the introduction, Section ", Ref("intro.sec", Text(" we ...", End)))))))))) 
//Question 3.4
let buildLabelEnv (s : Latex<'a>) = 
    let rec create (p : Latex<'a>) (map : Map<string, string>) prevNum =
        match p with
        | Label(s, t) -> create t (Map.add s prevNum map) prevNum
        | Section (s, a, t) | Subsection (s,a,t) -> create t map a 
        | Text(s,t) | Ref(s,t) -> create t map prevNum
        | End -> map

    create (addSecNumbers s) Map.empty ""

//Question 3.5
let toString (s : Latex<'a>) = 
    let labelEnv = buildLabelEnv s
    let nl = System.Environment.NewLine
    
    let rec createString p st = 
        match p with
        | Section(s, a, t) -> createString t (st + nl + a + " " + s + nl)
        | Subsection(s,a,t) -> createString t (st + nl + a + " " + s + nl)
        | Label(s,a) -> createString a st
        | Text(s,a) -> createString a (st + s)
        | Ref(s,a) -> createString a (st + (Map.find s labelEnv))
        | End -> st

    createString s ""



//Question 4.1
let mySeq = Seq.initInfinite (fun i -> if i % 2 = 0 then -i else i)

//Seq int
//seq<int> = seq [0; 1; -2; 3; ...]


//Question 4.2
let finSeq n M = 
    Seq.init (n+2*M) (fun f -> n + (2 * f))



//Question 4.3
type X = A of int | B of int | C of int * int

let rec zX xs ys = 
    match (xs,ys) with
    | (A a::aS, b::bS) -> C(a,b) :: zX aS bS
    | ([],[]) -> []
    | _ -> failwith "Error"

let rec uzX xs =
    match xs with
    | C(a,b)::cS -> let (aS, bS) = uzX cS 
                    (A a::aS, B b::bS)
    | [] -> ([],[])
    | _ -> failwith "Error"


//zX has the type X list -> int list -> X list
//This function joins a X list (more specifically A list, otherwise method fails) and int list (otherwise method fails here as well) into a list made of tuples of the values of the two given lists, resulting in an X list. More precisely a C list.
//If an empty list is given, an pair of empty lists are returned. 

//Example 1
zX  [A 1; A 1; A 1] [2;2;2];;
val it : X list =[C (1,2); C (1,2); C (1,2)]

//Example 2
zX  [A 1; A 2; A 3] [ 1; 2; 3];;
val it : X list = [C (1,1); C (2,2); C (3,3)]

//Example 3
zX  [A 3; A 2; A 1] [ 1; 2; 3];;
val it : X list = [C (3,1); C (2,2); C (1,3)]

//uzX has the type X list -> X List * X list
//This function takes an X list, aC list more specifically, otherwise the method fails or returns a pair of empty lists.
//Example 1
uzX [C (3,1); C (2,2); C (1,3)]
val it : X list * X list = ([A 3; A 2; A 1], [B 1; B 2; B 3])

//Example 2
uzX [C (1,1); C (2,2); C (3,3)];;
val it : X list * X list = ([A 1; A 2; A 3], [B 1; B 2; B 3])

//Example 3
uzX [C (1,2); C (1,2); C (1,2)];;
val it : X list * X list = ([A 1; A 1; A 1], [B 2; B 2; B 2])