﻿
(* Exercise 1*)


type Multiset<'a when 'a : equality> = ('a * int) list

let rec inv (multiSet: Multiset<'a>) = 
    match multiSet with
    | (a,x)::xs -> if List.exists (fun (y,z) -> y = a) xs then false else if x > 0 then inv xs else false
    | [] -> true


let rec insert (a:'a) i (set:Multiset<'a>) = 
    match set with
    | (b,x)::xs -> if b = a then (b, x+i)::xs else insert a i set
    | [] -> (a, i)::set


let rec numberOf (a:'a) (set:Multiset<'a>) =
    match set with
    | (x,i)::xs -> if x = a then i else numberOf a xs
    | [] -> 0


let rec delete  (e: 'a) (set:Multiset<'a>) =
    match set with
        | (b,x)::xs -> if b = e then xs else delete e (set@[(b,x)])
        | [] -> set

let rec union (set1:Multiset<'a>) (set2:Multiset<'a>) = 
    match set2 with
    | (a,i)::xs -> union (insert a i set1) xs
    | [] -> set1


type MultisetMap<'a when 'a : comparison> = Map<'a, int>

let invMap (set:MultisetMap<'a>) =
    Map.forall (fun key value -> value > 0) set

let insertMap (a:'a) i (set:MultisetMap<'a>) =
    if Map.containsKey a set then  Map.add a ((Map.find a set)+ i) set else Map.add a i set 

let unionMap (set1:MultisetMap<'a>) (set2:MultisetMap<'a>) =
    Map.fold (fun set key value -> insertMap key value set) set1 set2



(*Exercise 2*)
//let rec f i = function
 //             | [] -> []
 //             | x::xs -> (i,x)::f (i*i) xs;;

type 'a Tree = | Lf
               | Br of 'a Tree * 'a Tree * 'a Tree;;

let rec g p = function 
              | Lf      -> None
              | Br(_,a,t) when p a -> Some t
              | Br(t1, a, t2) -> match g p t1 with
                                    | None -> g p t2
                                    | res -> res;;

(* f's type is int -> 'a list -> (int 'a) list  *)
//Returns a list where every element in the initially given list has an added integer. This integer is mutplied by itself for every element in the inititally given list, starting on the value of the inititally specified integer.

(* g's type is ('a Tree -> bool) -> 'a Tree ->  'a Tree option *)
//The function returns a None if you give it a tree with a Left leaf (Lf), otherwise if the middle tree (a) of the given tree sastifies the given function (p) the right side of the given tree is returned as Some 'a Tree. If the middle tree (a) does not satisfiy the given function (p), the function iterates through the tree until it can return either None or Some 'a Tree

//opgave 2
let rec f i = function
              | [] -> []
              | x::xs -> (i,x)::f (i*i) xs;;

let rec fa i (list: 'a list) acc = 
    match list with
    | [] -> acc
    | x::xs -> fa (i*i) xs (acc@[(i,x)])

let rec ft i (list : 'a list) p =
    match list with
    | [] -> p
    | x::xs -> ft (i*i) xs (fun res -> p((i,x)::res))//(fun res -> p( (i,x)::res ) ))

//3

let rec h f (n,e) = match n with
                    | 0 -> e 
                    | _ -> h f (n-1, f n e);;

let A = Seq.initInfinite id;;

let B = seq { for i in A do
                for j in seq {0 .. i} do
                    yield (i,j) };;

let C = seq { for i in A do 
                for j in seq {0 .. i} do
                    yield (i-j, j)};;

let X = Seq.toList (Seq.take 4 A)
let Y = Seq.toList (Seq.take 6 B)
let Z = Seq.toList (Seq.take 10 C)


//3.1
// 24

//3.2
//(int  -> 'a -> 'a) -> int  * 'a -> 'ax






(*Exercise 3*)


let maxL list =
    let rec getLargest list largest =
        match list with
        | x::xs -> if x > largest then getLargest xs x else getLargest xs largest
        | [] -> largest

    getLargest list 0




type Title = string

type Section = Title * Elem list
and Elem = Par of string | Sub of Section

type Chapter = Title * Section list
type Book = Chapter list

(* Test code*)
let sec11 = ("Bakground", [Par "bla"; Sub(("Why programming", [Par "Bla."]))]);;

let sec12 = ("An example", [Par "bla"; Sub(("Special features", [Par "Bla."]))]);;
let sec21 = ("Fundamental on concepts",[Par "bla"; Sub(("Mathematical background", [Par "Bla."]))]);;
let sec22 = ("Operational semantics",[Sub(("Basics", [Par "Bla."])); Sub(("Applications", [Par "Bla."]))]);;
let sec23 = ("Further reading", [Par "bla"]);;
let sec31 = ("Overview", [Par "bla"]);;
let sec32 = ("A simple example", [Par "bla"]);;
let sec33 = ("An advanced example", [Par "bla"]);;
let sec41 = ("Status", [Par "bla"]);;
let sec42 = ("What's next?", [Par "bla"]);;
let ch1 = ("Introdution", [sec11;sec12]);;
let ch2 = ("Basic Issues", [sec21;sec22;sec23]);;
let ch3 = ("Advanced Issues", [sec31;sec32;sec33]);;
let ch4 = ("Conclusion", [sec41;sec42]);;

let book1 = [ch1;ch2;ch3;ch4]

let overview (book:Book) = 
    
    let rec getOverview (book:Book) finalList =
        match book with
        | (t, sList)::xs -> getOverview xs (finalList@[t])
        | [] -> finalList

    getOverview book []



let rec depthSection (section: Section) =
    let rec ittDepth list c = 
        match list with
        | Par(s)::xs -> ittDepth xs c
        | Sub(s)::xs -> let s = (depthSection s)
                        if s > c then ittDepth xs s else ittDepth xs c
        | [] -> c

    match section with
    | (t, []) -> 1
    | (t, eList) ->  (ittDepth eList 0) + 1


let depthElem  (elem:Elem) =
    match elem with
    | Par(s) -> 1
    | Sub(s) -> depthSection s

let depthChapter (chap : Chapter) =
    let rec iterateThrough list c = 
        match list with
        | x::xs -> let s = depthSection x
                   if s > c then iterateThrough xs s else iterateThrough xs c
        | [] -> c

    match chap with
    | (T, []) -> 1
    | (T, sList) -> (iterateThrough sList 0) + 1
    
let depthBook (book : Book) = 
    let rec iterateThrough list c = 
        match list with
        | x::xs -> let s = depthChapter x
                   if s > c then iterateThrough xs s else iterateThrough xs c
        | [] -> c

    iterateThrough book 0
       
        
// Exercises 4

type Numbering = int list
type Entry = Numbering * Title
type Toc = Entry list

let tocB (book:Book) = 

    let rec iterateElements list start count finalList =
        match list with
        | Par(s)::xs -> iterateElements xs start count finalList
        | Sub((t,s))::xs -> iterateElements xs start (count+1) (finalList@[(start@[count], t)])
        | [] -> finalList

    let rec iterateSections list start count finalList = 
        match list with
        | (t, l)::xs -> iterateSections xs start (count+1) ((finalList@ [([start;count], t)] )@(iterateElements l [start;count] 1 [] ))
        | [] -> finalList

    let rec iterateChapters list finalList count =
        match list with
        | (t,list)::xs -> iterateChapters xs ((finalList@[([count], t)])@iterateSections list count 1 []) (count+1)
        | [] -> finalList

    iterateChapters book [] 1
         
    

