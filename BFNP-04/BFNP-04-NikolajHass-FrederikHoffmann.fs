﻿module Exercise4


(* Exercise 4.1 9.1 *)
let xs = [1;2]
let rec g = function
    | 0 -> xs
    | n -> let ys = n::g(n-1)
           List.rev ys;;

g 2
(*
g2
- (g n, [n -> 2])
- (n::g(n-1),    [n->2, ys -> [])
- (2::g(n-1),    [n->2, ys -> [2])
- (g(n-1)::2),   [n->1, ys -> [1;2])
- (1::g(n-1)::2, [n->1, ys -> [2;1]])
- (2::xs::1),    [n->0, ys -> [2;1]])
- (1::1;2::2),   [n->0, ys -> [1;1;2;2]

It has been considered 

Whenever a new binding occurs a stackframe is added to the 
stack. Since lists are stored in the heap each new binding 
will change the stack. While cons does not need duplicates 
in the heap .rev will. So each time rev is called, the garbage 
collector will have to remove the old heap. Since duplicates
indicate that some memory(pointers) are unreachable.
*)

(* Exercise 4.2 9.3 *)

let rec sum m n = 
    match n with
    | 0 -> m
    | n -> m+n  + (sum m (n-1))
    

let rec sum2 m n a = 
    match n with
    | 0 -> a+m
    | n -> sum2 m (n-1) (a+n+m)

(* Exercise 4.3 9.4 *)

let rec listLength l a =
    match l with 
    | [] -> 0+a
    | x::l -> listLength l (a+1)



(* Exercise 4.4 9.6 *)

//From section 9.4

let rec factA = function
    | (0, m) -> m
    | (n, m) -> factA(n-1,n*m)


// Function using cont's
let rec factB = function
    | (0, m) -> m 1
    | (n, m) -> factB (n-1, (fun res -> m (n * res)) )
    


(* Exercise 4.5 8.6 *)

let F n = 
    let mutable n1 = 0
    let mutable n2 = 0
    let mutable s = n
    while(s > 0) do
         let tmp = n1 
         n1 <- (s + n2)
         n2 <- tmp
         s <- (s-1)
    
    (n1+n2)


(* Exercise 4.6 9.7 *)

//1. 
let rec fibA n n1 n2 = 
    match n with
    | 0 -> n1
    | n -> fibA (n-1) (n2+n1) n1

//2. 
let rec fibC n f =
    match n with 
    | 0 -> f 0
    | 1 -> f 1
    | n -> fibC (n-1) (fun res -> fibC (n-2) (f << (fun y -> y+res)))
    
//Needs to be updated
//As our hardware is too fast to see any diference (Everything takes 0.000 seconds, we have made the following test functions
for i = 1 to 100000 do let _ = F i in ()
//Result from above: Real: 00:00:03.221, CPU: 00:00:03.234,

for j = 1 to 100000 do let _ = fibA j 0 0 in ()
//Result from above: Real: 00:00:03.604, CPU: 00:00:03.593

for k = 1 to 1000 do let _ = fibC 25 (fun f -> f) in ()
//Result from above: Real: 00:00:07.408, CPU: 00:00:07.406
//Notice the difference in iterations.

(*It is clear that the recursive accumulating parameter functions is the fastest to execute, 
however it takes nearly the double of CPU time compared to the while loop. 
Furthermore, it can be noticed that the function which uses a continuation is the slowest to compute. 

While the continuation based function is the slowest, it saves us memory when executing.
*) 


(* Exercise 4.7 9.8*)
//From Exercise 3
type 'a BinTree =
    Leaf
  | Node of 'a * 'a BinTree * 'a BinTree

//Made to test easily
let intBinTree = Node(43, Node(25, Node(56,Leaf, Leaf), Leaf), Node(562, Leaf, Node(78, Leaf, Leaf)))


let rec countA binTree a = 
    match binTree with
    | Leaf -> a
    | Node(n, treeL, treeR) -> countA treeL (countA treeR (a+1))


(* Exercise 4.8 9.9 *)

let rec countAC binTree a c = 
    match binTree with
    | Leaf -> c a
    | Node(n, treeL, treeR) -> countAC treeL (a+1) (fun res -> countAC treeR res c)
    
           
(* Exercise 4.9 9.10 *)

let rec bigListK n k = 
    if  n = 0 then k []
    else bigListK (n-1) (fun res -> 1::k(res))

(* The reason behind bigListK's stackoverflow exception is due to the fact that function does not use tail recursion
and therefore uses up too much memory (fills up the stack). A similar example can be found in the book on page 212-213. 
Looking at the code example in the book, it can be seen that it is quite a simple change in order to make bigListK 
tail recursive: (fun res -> 1::k(res)) should be (fun res k(1::res))

 *)

(* Exercise 4.10 9.11 *)

let rec leftTree n a n2 = 
    match n with
    | 0 -> Node(n2, a, Leaf)
    | n -> leftTree (n-1) (Node(n2, a, Leaf)) (n2+1)

let rec rightTree n a n2 =
    match n with
    | 0 -> Node(n2, Leaf, a)
    | n -> rightTree (n-1) (Node(n2, Leaf, a)) (n2+1)


//1

//Below function is used for testing and is taken from page 214 in the course book.
let rec count = function
    | Leaf -> 0
    | Node( n,tl, tr) -> count tl + count tr + 1

(*  

    It has been possible to create a leftTree with the size 100000 and use countA on this without a stack overflow.
    However, using a rightTree with the same size gives a stack overflow. However, if the rightTree is reduced to
    10000 (one 0 less) it is possible to use countA. 

    The count function fails at a rightTree of the size of 100000 but terminates fine with the size 10000 similar to countA. 
    However count fails with a leftTree of the size 100000, where countA still functions. 

    There is a clear difference when using count as opposed to countA on a leftTree. 
*)

//2. 

//Below function is used for testing and is taken from page 215 in the course book. 
let rec countC t c = 
    match t with
    | Leaf -> c 0
    | Node(n, tl, tr) -> 
        countC tl (fun vl -> countC tr (fun vr-> c(vl+vr+1)))

(*
    There has been conducted test using a leftTree and a rightTree. 
    The size of the leftTree and rightTree was 10000000.

    leftTree runtime:
        countC:
            Real: 00:00:01.534, CPU: 00:00:01.531, GC gen0: 76, gen1: 27, gen2: 0
        countAC:
            Real: 00:00:01.321, CPU: 00:00:01.312, GC gen0: 26, gen1: 25, gen2: 0

    rightTree runtime:
        countC:
            Real: 00:00:02.850, CPU: 00:00:03.109, GC gen0: 52, gen1: 48, gen2: 1
        countAC:
            Real: 00:00:00.471, CPU: 00:00:00.468, GC gen0: 50, gen1: 1, gen2: 0
    
    As it can be seen from the above results that countAC is faster than countC. 
    countAC is only remotely faster than countC when dealing with a leftTree, however the 
    difference in performance really shows when the functions are used on a rightTree, where 
    the countC is up to 2,5 seconds slower than the countAC function. 
    
*)
    
(* Exercise 4.11 11.1 *)


//Trying to hide the shameless copying from the book page 259. 
let oddFilter sq = Seq.filter (fun n -> n % 2 <> 0) sq

let oddSeq = oddFilter (Seq.initInfinite(fun i -> i+1))

(* Exercise 4.12 11.2 *)
//Factorial function borrowed from the book
let rec fact = function
    | 0 -> 1
    | n -> n * fact(n-1)


let factSeq = Seq.initInfinite(fun i -> fact i)
 