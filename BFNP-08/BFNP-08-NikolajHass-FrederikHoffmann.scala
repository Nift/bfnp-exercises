sealed abstract class Expr
case class CstI(value: Int) extends Expr
case class Prim(op: String, e1: Expr, e2: Expr) extends Expr
case class Var(value: String) extends Expr

object test {
  def main(args: Array[String]) {
  	//Tests for exercise 1 i
    /*
    val vPlus = Prim("+", CstI(1), CstI(2))
    val vMinus = Prim("-", CstI(1), CstI(2))
    val vTimes = Prim("*", CstI(1), CstI(2))

    val test1 = Prim("*", CstI(7), Prim("+", Prim("-", CstI(10), CstI(6)), CstI(5)))
    val test2 = Prim("*", CstI(7), Prim("+", Prim("<", CstI(10), CstI(6)),CstI(5)))
    val test3 = Prim("<", CstI(1), CstI(2))
    val test4 = Prim("<", CstI(2), CstI(1))
    val test5 = Prim(">", CstI(2), CstI(1))
    val test6 = Prim(">", CstI(1), CstI(2))
    val test7 = Prim("&", test3, test5)
    val test8 = Prim("&", test3, test6)
    val test9 = Prim("/", CstI(10), CstI(2))


    println("plus " + eval0(vPlus))
    println("minus " + eval0(vMinus))
    println("times " + eval0(vTimes))
    println("test1 " + eval0(test1))
    println("test2 " + eval0(test2))
    println("test3 " + eval0(test3))
    println("test4 " + eval0(test4))
    println("test5 " + eval0(test5))
    println("test6 " + eval0(test6))
    println("test7 " + eval0(test7))
    println("test8 " + eval0(test8))
    println("test9 " + eval0(test9))*/
    
    //Exercise 8.1 ii tests
    /*
    val testE1 = Prim("+", Var("x"), CstI(2))
    val map1 = Map("x" -> 27)
    println("testE1 " + eval1(testE1, map1))

    val testE2 = Prim("+", Var("x"), Var("y"))
    val map2 = Map("x" -> 27, "y" -> 42)
    println("testE2 " + eval1(testE2, map2))


    val testE3 = Prim("+", Var("x"), Var("y"))
    val map3 = Map("x" -> 27)
    println("testE3 " + eval1(testE3, map3))
*/
/*
	
	val testSim1 = Prim("+", CstI(1), CstI(0))
	var testSim2 = Prim("+", Var("x"), CstI(0))
	var testSim3 = Prim("+", Var("x"), CstI(1))

	val testSim4 = Prim("-", CstI(1), CstI(0))
	var testSim5 = Prim("-", Var("x"), CstI(0))
	var testSim6 = Prim("-", Var("x"), CstI(1))
	var testSim7 = Prim("-", CstI(0), CstI(1))
	var testSim8 = Prim("-", CstI(0), Var("x"))
	var testSim9 = Prim("-", CstI(1), Var("x"))

	println("testSim1 " + simplify(testSim1))
	println("testSim2 " + simplify(testSim2))
	println("testSim3 " + simplify(testSim3))
	println("testSim4 " + simplify(testSim4))
	println("testSim5 " + simplify(testSim5))
	println("testSim6 " + simplify(testSim6))
	println("testSim7 " + simplify(testSim7))
	println("testSim8 " + simplify(testSim8))
	println("testSim9 " + simplify(testSim9))
	*/

	val test1E2 = Prim("*", CstI(7), Prim("+", Prim("-", CstI(10), CstI(6)), CstI(5)))
    val test2E2 = Prim("*", CstI(7), Prim("+", Prim("<", CstI(10), CstI(6)),CstI(5)))
    val test3E2 = Prim("<", CstI(1), CstI(2))
    val test4E2 = Prim("<", CstI(2), CstI(1))
    val test5E2 = Prim(">", CstI(2), CstI(1))
    val test6E2 = Prim(">", CstI(1), CstI(2))
    val test7E2 = Prim("&", test3E2, test5E2)
    val test8E2 = Prim("&", test3E2, test6E2)
    val test9E2 = Prim("/", CstI(10), CstI(2))
    val test10E2 = Prim("/", CstI(10), CstI(0))

    val map1 = Map("x" -> 27)
    val test11E2 = Prim("+", Var("x"), CstI(2))
    val test12E2 = Prim("-", Var("x"), CstI(2))
    val test13E2 = Prim("*", Var("x"), CstI(2))
    val test14E2 = Prim("/", Var("x"), CstI(2))

    val map2 = Map("x" -> 27, "y" -> 42)
    val test15E2 = Prim("+", Var("x"), Var("y"))
    val test16E2 = Prim("-", Var("x"), Var("y"))
    val test17E2 = Prim("*", Var("x"), Var("y"))
    val test18E2 = Prim("/", Var("x"), Var("y"))

    val map3 = Map("x" -> 27, "y" -> 0)
 
    println("test1E2 " + eval2(test1E2, map1))
    println("test2E2 " + eval2(test2E2, map1))
    println("test3E2 " + eval2(test3E2, map1))
    println("test4E2 " + eval2(test4E2, map1))
    println("test5E2 " + eval2(test5E2, map1))
    println("test6E2 " + eval2(test6E2, map1))
    println("test7E2 " + eval2(test7E2, map1))
    println("test8E2 " + eval2(test8E2, map1))
    println("test9E2 " + eval2(test9E2, map1))
    println("test10E2 " + eval2(test10E2, map1))
    println("test11E2 " + eval2(test11E2, map1))
    println("test12E2 " + eval2(test12E2, map1))
    println("test13E2 " + eval2(test13E2, map1))
    println("test14E2 " + eval2(test14E2, map1))


    println("test15E2 " + eval2(test15E2, map2))
    println("test16E2 " + eval2(test16E2, map2))
    println("test17E2 " + eval2(test17E2, map2))
    println("test18E2 " + eval2(test18E2, map2))
    
    println("test18E2 map3 " + eval2(test18E2, map3))


  }


	def eval0(expr: Expr): Int = {
			expr match {
			case CstI(i) => i
			case Prim(op, e1, e2) =>
				val v1 = eval0(e1)
				val v2 = eval0(e2)
				op match {
					case "+" => v1 + v2
					case "-" => v1 - v2
					case "*" => v1 * v2
					//Exericse 8.1 i
					case "/" => v1 / v2
					case "<" => if( v1 < v2) 1 else 0
					case ">" => if ( v1 > v2) 1 else 0
					case "&" => if (v1 == 1 & v2 == 1) 1 else 0

				}
		}
	}
	//Exercise 8.1 ii
	def eval1(expr: Expr, env: Map[String, Int]): Int = {
			expr match {
			case CstI(i) => i
			case Var(x) => env get x match{
				case Some(i) => i
				case None => 0 //Set it to 0 as this eval is not made to deal with option types yet. 	
			}
			case Prim(op, e1, e2) =>
				val v1 = eval1(e1, env)
				val v2 = eval1(e2, env)
				op match {
					case "+" => v1 + v2
					case "-" => v1 - v2
					case "*" => v1 * v2
					case "/" => v1 / v2
					case "<" => if( v1 < v2) 1 else 0
					case ">" => if ( v1 > v2) 1 else 0
					case "&" => if (v1 == 1 & v2 == 1) 1 else 0

				}
		}
	}

	//Exercise 8.2
	//def eval1(expr: Expr, env: Map[String, Int]): Int = {
	def simplify(expr: Expr) : Expr = {
		expr match {
			case CstI(i) => CstI(i)
			case Var(x) => Var(x)
			case Prim(op, e1, e2) =>
			val v1 = simplify(e1)
			val v2 = simplify(e2)
			op match{
				case "*" => 
					v1 match{
						case CstI(i) => if (i == 0) {
											CstI(0)
										}
										else if(i == 1)
										{
											v2
										}
										else{
											v2 match {
											case CstI(j) => if (j == 0){
																CstI(0)
															}
															else if(j==1){
																v1
															}
															else{
																Prim(op, v1,v2)
															}
													}
												}
						case Var(x) => 
							v2 match{
								case CstI(i) => if(i == 0){
												CstI(0)
												}
												else if(i == 1){
													v1
												}
												else{
													Prim(op, v1, v2)
												}
								case Var(y) => Prim(op, v1, v2)
							}
											}
				case "+" =>
					v1 match {
						case CstI(i) => if (i == 0) {
											v2 
										}
										else{ 
											v2 match {
												case CstI(i) => if(i == 0) v1 else Prim(op, v1, v2) }}
						case Var(x) => 
							v2 match{
								case CstI(i) => if(i == 0){
									v1
								}
								else{
									Prim(op, v1,v2)
								}
								case Var(y) => Prim(op, v1,v2)
							}

					}
				case "-" => 
					v1 match {
						case CstI(i) =>
								if(i == 0){
									v2 match{
										case CstI(j) => if(j != 0) { Prim("*", CstI(-1), v2)} else { CstI(0)}

										case Var(y) => Prim("*", CstI(-1), v2)
									}
								} 
								else{
									v2 match{
										case CstI(j) => if(j == 0) { v1 } else {Prim(op, v1, v2)}
										case Var(y) => Prim(op, v1, v2)
										}								
									}
						case Var(x) => 
							v2 match{
								case CstI(j) => 
									if(j == 0){
										v1
									}
									else{
										Prim(op, v1, v2)
									}
								case Var(y) => 
									if(x == y)
									{
										CstI(0)
									}
									else
									{
										Prim(op, v1, v2)
									}
							}


					}
			}
		}
	}

	//Exercise 8.2
	def eval2(expr: Expr, env: Map[String, Int]): Option[Int] = {
			expr match {
			case CstI(i) => Some(i)
			case Var(x) => env get x match{
				case Some(i) => Some(i)
				case None => None //Set it to 0 as this eval is not made to deal with option types yet. 	
			}
			case Prim(op, e1, e2) =>
				val v1 = eval1(e1, env)
				val v2 = eval1(e2, env)
				op match {
					case "+" => Some(v1 + v2)
					case "-" => Some(v1 - v2)
					case "*" => Some(v1 * v2)
					case "/" => 
							if(v2 == 0){
								None
							}
							else{
								Some(v1 / v2)
							}
								
					case "<" => if( v1 < v2) Some(1) else Some(0)
					case ">" => if ( v1 > v2) Some(1) else Some(0)
					case "&" => if (v1 == 1 & v2 == 1) Some(1) else Some(0)

				}
		}
	}
	
}
