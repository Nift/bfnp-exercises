object PersonExample {
  trait Counter {
    private var count = 0
    def increment() { count += 1 }
    def getCount = count
  }

  abstract class Person(val name: String) {
    def print()
  }

  class CountingPerson(override val name: String) extends Person(name) with Counter {
    def print() {
      increment()
      println(name + " has been printed " + getCount + " times")
    }
  }

  class Student(override val name: String, val programme: String) extends Person(name) {
    def print() {
      println(name + " studies " + programme)
    }
  }

  def main(args: Array[String]) {
    val p: Person = new Student("Ole", "SDT");
    p.print()
    p.print
    println(p.name)

    // Anonymous subclass and instance.
    val s = new Student("Kasper", "SDT") {
      override def print() { 
	super.print() 
	println("and does much else")
      }
    }
    s.print()

    // Using traits
    val q1: Person = new CountingPerson("Hans")
    val q2: Person = new CountingPerson("Laila")
    q1.print(); q1.print(); 
    q2.print(); q2.print(); q2.print()

  }
}

