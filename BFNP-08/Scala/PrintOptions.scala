object PrintOptions {
  def main(args: Array[String]): Unit = {
    System.out.println("Options selected:")
    for (arg <- args)
      if (arg.startsWith("-"))
	System.out.println(" "+arg.substring(1))
    System.out.println("Options selected again:")
    for (arg <- args; if (arg.startsWith("-")))
      System.out.println(" "+arg.substring(1))
  }
}
