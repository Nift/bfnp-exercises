object OptionExample {
  def sqrt(x: Double): Option[Double] =
    if (x<0) None else Some(math.sqrt(x))


  def mul3(x: Option[Double]) =
    x match {
      case None    => None
      case Some(v) => Some(3*v)
    }

  def mul_3(x: Option[Double]) =
    for ( v <- x )
      yield 3*v

  def main(args: Array[String]) {
    val ex01 = sqrt(42.2)
    println("ex01 = " + ex01)

    val ex02 = mul3(Some(43.2))
    println("ex02 = " + ex02)

    val ex03 = mul3(Some(43.2))
    println("ex03 = " + ex03)

    val primes = List(3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59,61,67)
    val ex04 = for (x <- primes; if x*x < 100) yield 3*x
    println("ex04 = " + ex04)

    val primesOpt = Some(List(3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59,61,67))
    for (l <- primesOpt) {
      val ex05 = for (x <- l; if x*x < 100) yield 3*x
      println("ex05 = " + ex05)
    }

    val primesOpt2 = List(Some(3),None,Some(5),Some(7),None,Some(11),Some(13))
    for (l <- primesOpt2) {
      val ex06 = for (x <- l; if x*x < 100) yield 3*x
      println("ex06 = " + ex06)
    }

    val ex07 = (for (x <- 1 to 200; if x%5!=0 && x%7!=0)
                 yield 1.0/x).foldLeft (0.0) (_+_)

    println("ex07 = " + ex07)

    val ex08 = for (i <- 1 to 10; j <- 1 to i) 
                 yield (i,j)
    println("ex08 = " + ex08)

  }
}
