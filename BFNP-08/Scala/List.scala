object ListExample {

  def sum(xs: List[Int]): Int = 
    xs match {
      case Nil   => 0
      case x::xr => x + sum(xr)
    }

  def sum1(xs: List[Int]) = 
    xs.foldLeft(0)((res,x)=>res+x)

  def sum2(xs: List[Int]) = 
    xs.foldLeft(0)(_+_)

  def repeat[T](x: T, n: Int): List[T] = 
    if (n==0) 
      Nil 
    else
      x :: repeat(x, n-1)

  def foreach1[T](xs: List[T], act: T=>Unit): Unit =
    xs match {
      case Nil   => { }
      case x::xr => { act(x); foreach1(xr, act) }
    }

  def main(args: Array[String]) {
    val ex01 = List(1,2,3)
    println(ex01)
    val ex02 = 1 :: 2 :: 3 :: Nil
    println(ex02)
    val ex03 = List("foo", "bar")
    println(ex03)
    val ex04 = List(("Peter", 1962), ("Lone", 1960))
    println(ex04)

    // Four ways to loop
    val xs = ex01
    for (x <- xs) println(x)

    xs foreach { x => println(x) }  // Actual meeting of for-expression

    xs.foreach(println)

    xs foreach println

    // Three ways ot sum
    var res = 0
    for (x <- xs) res += x 
    println(res)

    res = 0
    xs foreach { x => res += x } 
    println(res)

    res = 0
    xs foreach { res += _ } 
    println(res)

    println(sum1(xs))

    println(sum2(xs))

    // Sum function
    println(sum(xs))

    // Repeat element n times.
    println(repeat("abc", 4))

    // Foreach
    foreach1(xs, {x:Int => println(x)})

  }
}
