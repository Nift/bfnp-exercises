object ExprExample {

  sealed abstract class Expr
  case class CstI(value: Int) extends Expr
  case class Prim(op: String, e1: Expr, e2: Expr) extends Expr

  def eval(e: Expr): Int = 
    e match {
      case CstI(i) => i
      case Prim(op, e1, e2) =>
	val v1 = eval(e1)
	val v2 = eval(e2)
	op match {
	  case "+" => v1 + v2
	  case "*" => v1 * v2
	  case "/" => v1 / v2
	}
    }

  def main(args: Array[String]) {
    val ex01 = Prim("+", CstI(7), Prim("*", CstI(9), CstI(10)))
    println("eval ex01 = " + eval(ex01))
  }
}
