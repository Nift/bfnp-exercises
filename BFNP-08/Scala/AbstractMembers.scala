// Section 5.2 Abstract members
object AbstractMembers {

  abstract class AbsCell {
    type T
    val init: T
    private var value: T = init
    def get: T = value
    def set(x:T) : Unit = { value=x }

    override def toString() = value.toString()
  }

  def reset(c:AbsCell):Unit = c.set(c.init)  

  def doAbstractMembers() {
    println("**5.2 Abstract Members**")
    val cell = new AbsCell {type T = Int; val init=1}
    println("cell="+cell)  // I can't explain why value is not set to 1 at this time?
    println("cell="+{cell.set(1);cell})
    cell.set(cell.get * 2)
    println("cell="+cell)

    reset(cell)
    println("after reset cell="+cell)
  }

  def main(args: Array[String]) {
    doAbstractMembers()
  }

}
