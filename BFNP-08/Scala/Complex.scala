object ComplexNumbers {
  class Complex(real: Double, imaginary: Double) {
    def re() = real
    def im() = imaginary
    override def toString() = "" + re() + (if (im<0) "" else "+") + im + "i"
  }

  class Complex2(real: Double, imaginary: Double) {
    def re = real
    def im = imaginary
  }

  def main(args: Array[String]) {
    val c = new Complex(1.2, 3.4)
    println("imagiary part: " + c.im())
    println("c: " + c)
    val c2 = new Complex2(1.2, 3.4)
    println("imagiary part: " + c2.im)
    println("c2: " + c2)
  }
}
