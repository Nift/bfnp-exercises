object VarianceExample {

  class T() {
    var n = "T"
    override def toString() = "type: " + n
  }
  class S() extends T {n="S"}

  class C[+T](x: T) {
    def outputT: T = x
    override def toString() = x.toString()
  }

  class D[-T](x: T) {
    def inputT(y: T) { }
    override def toString() = x.toString()
  }

  def main(args: Array[String]) {

    // Co-variance
    val exCT = new C[T](new T())
    println("exCT = " + exCT)
    val exCS = new C[S](new S())
    println("exCS = " + exCS)
    val exCTs:List[C[T]] = List(exCT,exCS)  // Will not compile if I remove +T
    println("exCTs = " + exCTs)
    for (c <- exCTs) println("elem = " + c.outputT)

    // Contra-variance
    val exDT = new D[T](new T())
    println("exDT = " + exDT)
    val exDS = new D[S](new S())
    println("exDS = " + exDS)
    val exDSs:List[D[S]] = List(exDT,exDS)  // Will not compile if I remove -T
    println("exDSs = " + exDSs)
  }
}
