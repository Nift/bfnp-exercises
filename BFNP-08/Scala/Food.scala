object FoodExample {

  class Food
  abstract class Animal {
    type SuitableFood <: Food
    def eat(food: SuitableFood)
  }

  class Grass extends Food
  class Cow extends Animal {
    type SuitableFood = Grass
    override def eat(food : SuitableFood) { }
  }

  class DogFood extends Food
  class Dog extends Animal {
    type SuitableFood = DogFood
    override def eat(food : SuitableFood) { }
  }

  def main(args: Array[String]) {
    val cow = new Cow()
    val dog = new Dog()
  }
}
