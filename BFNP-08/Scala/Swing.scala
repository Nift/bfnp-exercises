object SwingExample {

  import scala.swing._

  class FirstSwingApp extends SimpleSwingApplication {
    var c = 0
    val textArea = new TextArea("Hi there",1,40)
    val b = new Button {text = "Click me"
		        reactions += {case scala.swing.event.ButtonClicked(_) => 
				        textArea.text = ("Button clicked "+c+" times.")
				      println (c)
                                      c+=1}}

    def top = new MainFrame {
      title = "First Swing App"
      contents = new FlowPanel() {contents += b
                                  contents += textArea}
    }
  }

  def main(args: Array[String]) {
    val app = new FirstSwingApp()
    app.startup(args)
  }
}
