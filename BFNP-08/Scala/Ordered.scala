object OrderedExample {
  trait Ordered[A] extends java.lang.Comparable[A] {
    def compareTo(that: A): Int
    def <  (that: A): Boolean = (this compareTo that) <  0
    def >  (that: A): Boolean = (this compareTo that) >  0
    def <= (that: A): Boolean = (this compareTo that) <= 0
    def >= (that: A): Boolean = (this compareTo that) >= 0
  }

  class OrderedIntPair(val fst: Int, val snd: Int) extends Ordered[OrderedIntPair] { 
    def compareTo(that: OrderedIntPair): Int =
      if ((this.fst < that.fst) || ((this.fst==that.fst) && (this.snd<that.snd))) -1
      else (if ((this.fst == that.fst) && (this.snd == that.snd)) 0
            else 1)
  }


  def main(args: Array[String]) {
    val pair1 = new OrderedIntPair(3, 4)
    val pair2 = new OrderedIntPair(5, 4)
    if (pair1 > pair2) println("Greater") else println("Less")
  }

}
