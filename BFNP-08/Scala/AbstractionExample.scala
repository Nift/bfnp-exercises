object AbstractionExample {
  class GenCell[T](init: T) {
    private var value: T = init
    def get: T = value
    def set(x:T): Unit = {value=x}
    override def toString() = value.toString()
  }

//** 5.1 Functional Abstraction
  def swap[T](x: GenCell[T], y: GenCell[T]): Unit = {
    val t = x.get
    x.set(y.get)
    y.set(t)
  }

  // Parameter bounds
  trait Ordered[T] {
    def < (x:T): Boolean
  }

  def updateMax[T <: Ordered[Any]](c: GenCell[T], x: T) =
    if (c.get < x) c.set(x)

  class OrderedInt(self: Int) extends Ordered[Any] {
    def i = self
    def <(that:Any):Boolean = 
      that.isInstanceOf[OrderedInt] &&
      this.i < that.asInstanceOf[OrderedInt].i

    override def toString() = self.toString()
  }

  def doFunctionalAbstraction() {
    println("**doFunctionAbstraction**")
    val x = new GenCell(new OrderedInt(1))
    val y = new GenCell(new OrderedInt(2))
    println("x="+x+" and y="+y)
    println("x<y="+(x.get<y.get))
    swap(x,y)
    println("x="+x+" and y="+y)
    println("x<y="+(x.get<y.get))

    updateMax(x,new OrderedInt(50))  // It infers that T is OrderedInt is used, i.e., updateMax[OrderedInt] 
    println("updateMax(x,10)="+x)
  }

  class Person {
    val typ = "Person"
    override def toString() = typ
  }

  class Student extends Person {
    override val typ = "Student"
  }

  // Variance
  abstract class GenList[+T] { // Add + to make GenList co-variant
    def isEmpty: Boolean
    def head: T
    def tail: GenList[T]

    // Below does not work as T occurs in contravariant position. 
    // Intuitively: If I pass in x of type S, where S:<T, then I have 
    // converted a list of elements T to a list of elements S, which is unsound.
    //def prepend(x:T):GenList[T] = new Cons(x,this)

    // Below works because the result list will have type GenList[S] and not GenList[T].
    // The bound says that you can only convert GenList[T] to list of 
    // elements S where S>:T, i.e., elements that have all the featurs form S.
    def prepend[S >: T](x:S): GenList[S] = new Cons(x,this) 
  }

  object Empty extends GenList[Nothing] {
    def isEmpty = false
    def head = throw new Error("Empty.head")
    def tail = throw new Error("Empty.tail")
    override def toString() = "Nil"
  }

  object EmptyInt extends GenList[Int] {
    def isEmpty = false
    def head = throw new Error("Empty.head")
    def tail = throw new Error("Empty.tail")
    override def toString() = "Nil"
  }

  object EmptyString extends GenList[String] {
    def isEmpty = false
    def head = throw new Error("Empty.head")
    def tail = throw new Error("Empty.tail")
    override def toString() = "Nil"
  }

  class Cons[T](x:T, xs:GenList[T]) extends GenList[T] {
    def isEmpty = false
    def head = x
    def tail = xs
    override def toString() = head.toString() + "," + tail.toString()
  }

  import scala.reflect.runtime.{universe => ru}
  def getTypeTag[T: ru.TypeTag](obj: T) = ru.typeTag[T]  

  def doVariance() {
    println("\n**doVariance**")

    // This will not work without making GenList co-variant, i.e., add +
    // This is simply because Nothing is not the same as T.
    // If I make GenList co-variant, then we have GenList[Nothing]:<GenList[T] for all T because Nothing:<T for all T
    val xsInt01 = new Cons(1,Empty)  
    println("xsInt01="+xsInt01)
    val xsString01 = new Cons("S",Empty)
    println("xsString01="+xsString01)

    // Below will work eventhough GenList is not declared co-variant, because T is also an Int or a String
    val xsInt02 = new Cons(1,EmptyInt) 
    println("xsInt02="+xsInt02)
    val xsString02 = new Cons("S",EmptyString)
    println("xsString02="+xsString02)

    // Binary methods and lower bounds
    println("\n**Binary methods and lower bounds**")
    val xsInt03 = xsInt02.prepend(2)
    println("xsInt03="+xsInt03)

    val xsString03 = xsString02.prepend("T")
    println("xsString03="+xsString03)

    val xsLong01 = xsInt03.prepend(4:Long)
    // xsLong01 has type GenList[AnyVal] because Long is not a subtype of Int.
    println("xsLong01="+xsLong01 + " with type " + getTypeTag(xsLong01))    
    val xsLong02 = xsLong01.prepend(5)
    println("xsLong02="+xsLong02 + " with type " + getTypeTag(xsLong02))    
    val xsMix01 = xsLong02.prepend("S")
    // xsMix01 has type GenList[Any] because now we mix numbers and strings.
    println("xsMix01="+xsMix01 + " with type " + getTypeTag(xsMix01))    

    val xsPerson01 = new Cons(new Student, Empty)
    println("xsPerson01="+xsPerson01 + " with type " + getTypeTag(xsPerson01))    
    // Below is sound because I convert type to GenList[Person] of which it is ok we have a Student among
    val xsPerson02 = xsPerson01.prepend(new Person)
    println("xsPerson02="+xsPerson02 + " with type " + getTypeTag(xsPerson02))    
    // Below, the type remains to be GenList[Person]
    val xsPerson03 = xsPerson02.prepend(new Student)
    println("xsPerson03="+xsPerson03 + " with type " + getTypeTag(xsPerson03))    
  }

  def main(args: Array[String]) {
    doFunctionalAbstraction()
    doVariance()
  }
}
