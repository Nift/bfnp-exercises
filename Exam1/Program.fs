﻿type Name = string;;
type Score = int;;
type Result = Name * Score;;

let rec legalResults (list:Result list) =
    match list with
    | (name,score)::xs -> if score > -1 && score < 101 then legalResults xs else false
    | [] -> true


let maxScore (list:Result list)= 
    let rec getScore (list:Result list) largest =
        match list with
        | (name, score)::xs -> if score > largest then getScore xs score else getScore xs largest
        | [] -> largest

    getScore list 0


let best (list : Result list) =
    let rec bestResult list (n,sc) =
        match list with
        | (name, score)::xs -> if score > sc then bestResult xs (name,score) else bestResult xs (n,sc)
        | [] -> (n,sc)

    bestResult list ("", 0)


let average (list : Result list) =
    let count = list.Length
    let rec getNumbers (list : Result list) = 
        match list with
        | (n, sc)::xs -> sc + getNumbers xs
        | [] -> 0

    (getNumbers list) / count

let rec delete (r: Result) (rs : Result list) =
    match rs with
    | x::xs -> if x = r then xs else delete r (xs@[x])
    | [] -> rs
    

let bestN n (list: Result list)  =
    if list.Length < n then failwith "List too short"

    let rec getList (l: Result list) c (finalList : Result list) = 
        if c > 0 then 
            let b = best l
            getList (delete b l) (c-1) (finalList@[b])
        else
            finalList
    getList list n []   

    

// problem 2

type Typ = | Integer
           | Boolean
           | Ft of Typ list * Typ;;

type Decl = string * Typ;;

let rec distinctVars (list : Decl list) = 
    match list with
    | (xi,i)::xs -> if List.exists (fun (xj, j) -> xj = xi ) xs then false else distinctVars xs
    | [] -> true


type SymbolTable = Map<string, Typ>;;

let toSymbolTable (list : Decl list) =
    
    let rec runThrough list (s:SymbolTable)= 
        match list with
        | (x,y)::xs -> runThrough xs (s.Add(x,y))
        | [] -> s
    
    
    runThrough list Map.empty


let extendST (st:SymbolTable) (list : Decl list) =
    
    let rec runThrough l (s:SymbolTable) =
        match l with
        | (x,y)::xs -> runThrough xs (s.Add(x, y))
        | [] -> s

    runThrough list st


type Exp = | V of string
           | A of string * Exp list;;



let symbolsDefined (sym:SymbolTable) (e:Exp) = 

    let rec iterateList (st:SymbolTable) (eList: Exp list) = 
        match eList with
        | V(x)::xs -> Map.containsKey x st && iterateList st xs
        | [] -> true

    match e with
    | V(s) -> Map.containsKey s sym
    | A(s, e) -> Map.containsKey s sym && iterateList sym e



let rec typOf (sym: SymbolTable) (e:Exp) = 
    
    let whatReturnType s = 
        match s with
        | ">"  -> Boolean
        | "<" -> Boolean
        | _ -> Integer
    

    match e with
    | V(s) -> if Map.containsKey s sym 
                then
                    match Map.find s sym with
                    | Ft(e, t) -> 
                                match s with 
                                | ">" -> if Boolean = t then t else failwith "not well typed"
                                | "<" -> if Boolean = t then t else failwith "Not well typed"
                                | _ -> t
                    | e -> e
                else
                    failwith "Ill-typed"

    | A(s, e) -> if Map.containsKey s sym 
                    then 
                        match Map.find s sym with
                        | Ft(e, t) -> if whatReturnType s = t then t else failwith "Not well typed"
                        | e -> e
                   else 
                    failwith "Does not exist"



let tmp = Ft([Integer; Integer],Boolean)

let l : Decl list =
  [("x", Integer); ("y", Boolean); (">", Ft ([Integer; Integer],Boolean))]

let sym = toSymbolTable l

let test = V(">")


type Stm = | Ass of string * Exp
           | Seq of Stm * Stm
           | Ite of Exp * Stm * Stm
           | While of Exp * Stm
           | Block of Decl list * Stm

let rec wellTyped (sym:SymbolTable) (stm:Stm) =
    match stm with
    | Ass(x, e) -> Map.containsKey x sym && symbolsDefined sym e && typOf sym e = typOf sym (V(x))
    | Seq(stm1, stm2) -> wellTyped sym stm1 && wellTyped sym stm2
    | Ite(e, stm1, stm2) -> symbolsDefined sym e && typOf sym e = Boolean && wellTyped sym stm1 && wellTyped sym stm2


    
    
    

