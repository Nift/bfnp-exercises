﻿// Comple library: fsc -a VectorSimple.fsi VectorSimple.fs
//#r @"/Users/nielshallenberg/Dropbox/Documents/Work/ITU/Course/BFNP-F2013/Lectures/Source/Chapter7/Vector/VectorSimple.dll"
#if INTERACTIVE
#r @"C:\Users\Frederik\Documents\Visual Studio 2013\Projects\ConsoleApplication4\ConsoleApplication4\ComplexNumbers.dll"
#endif

open ComplexNumbers


let a = make(1,1)

let b = make(3,4)


let add = coord (a .+ b)

let sub = coord (a .- b)

let mul = coord (a .* b)

let div = coord (a ./ b)