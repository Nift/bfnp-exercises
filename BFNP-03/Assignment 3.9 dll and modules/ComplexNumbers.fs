﻿module ComplexNumbers
type Complex =
     | C of int * int
     static member ( .+ ) (C(a,b),C(c,d))     = C(a+c,b+d)
     static member ( .- ) (C(a,b),C(c,d))     = C(-a+c,-b+d)
     static member ( .* ) (C(a,b),C(c,d))     = C(a*c - b*d, b*c+a*d)
     static member ( ./ ) (C(a,b),C(c,d))     = match ((a,b),(c,d)) with
                                                      |((a,b),(c,d)) when a=0 && b=0 -> failwith "a and b cannot both be zero"
                                                      |(_,_) -> C((a/a*2+b*2), (-b/a*2+b*2))
                                                          
let make (a,b) = C(a,b)
let coord(C(x,y)) = (x,y)
