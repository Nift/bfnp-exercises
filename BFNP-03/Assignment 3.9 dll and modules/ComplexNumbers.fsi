﻿module ComplexNumbers
[<Sealed>]
type Complex =
     static member ( .+ ) : Complex * Complex -> Complex
     static member ( .- ) : Complex * Complex -> Complex
     static member ( .* ) : Complex * Complex -> Complex
     static member ( ./ ) : Complex * Complex -> Complex
val make : int * int -> Complex
val coord: Complex -> int * int