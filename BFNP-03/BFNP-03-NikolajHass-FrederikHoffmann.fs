﻿module Exercise3

﻿//3.1

type 'a BinTree =
    Leaf
  | Node of 'a * 'a BinTree * 'a BinTree

let intBinTree = Node(43, Node(25, Node(56,Leaf, Leaf), Leaf), Node(562, Leaf, Node(78, Leaf, Leaf)))

let rec traOrder tree =
    match tree with
    Leaf -> []
    | Node(n,treeL,treeR) ->
         traOrder treeL @ n::traOrder treeR;


//3.2

let rec mapInOrder f tree =
    match tree with
    Leaf -> Leaf
    | Node(n, treeL, treeR) -> 
        Node( f n, mapInOrder f treeL, mapInOrder f treeR)
        

//3.3

let floatBinTree = Node(43.0,Node(25.0, Node(56.0,Leaf, Leaf), Leaf), Node(562.0, Leaf, Node(78.0, Leaf,Leaf)))

let rec foldInOrder f v tree =
    match tree with 
    Leaf -> 0.0
    | Node(n, treeL, treeR) -> 
        f n v + foldInOrder f v treeL + foldInOrder f v treeR


//3.4


type aExp = (* Arithmetical expressions *)
| N of int (* numbers *)
| V of string (* variables *)
| Add of aExp * aExp (* addition *)
| Mul of aExp * aExp (* multiplication *)
| Sub of aExp * aExp (* subtraction *)

let rec A aExp state  = 
    match aExp with
    | N(n) -> n
    | V(s) -> Map.find s state
    | Add(a,b) -> (A a state) + (A b state)
    | Mul(a,b) -> (A a state) * (A b state)
    | Sub(a,b) -> (A a state) - (A b state)
    
    
type bExp = (* Boolean expressions *)
| TT (* true *)
| FF (* false *)
| Eq of aExp * aExp (* equality *)
| Lt of aExp * aExp (* less than *)
| Neg of bExp (* negation *)
| Con of bExp * bExp (* conjunction *)

let rec B bExp state =
    match bExp with
    | TT -> true
    | FF -> false
    | Eq(a,b) -> (A a state) = (A b state)
    | Lt(a,b) -> (A a state) < (A b state)
    | Neg(bExp) -> not (B bExp state)
    | Con(a,b) -> (B a state)&&(B b state)


type stm = (* statements *)
| Ass of string * aExp (* assignment *)
| Skip
| Seq of stm * stm (* sequential composition *)
| IT of bExp * stm (*if-then*)
| ITE of bExp * stm * stm (* if-then-else *)
| While of bExp * stm (* while *)
| RepUn of bExp * stm  (*repeat-until*)

let update x v s = Map.add x v s

let rec I stm state =
    match stm with
    | Ass(x,a) -> update x (A a state) state
    | Skip -> state
    | Seq(stm1, stm2) -> I stm1 state |> I stm2 
    | ITE(b,stm1,stm2) -> if B b state then I stm1 state else I stm2 state
    | While(b, stm) -> I (ITE(b, (Seq(stm, (While(b, stm)))), Skip)) state
    (*3.5*)
    | IT(b, stm1) -> I (ITE(b, stm1, Skip)) state  
    | RepUn(b, stm) -> I (Seq(stm, (While(Neg(b), stm)))) state
    


let fac = Seq(Ass("y", N 1),
            While(Neg(Eq(V "x", N 0)),
                Seq(Ass("y", Mul(V "x", V "y")) ,
                    Ass("x", Sub(V "x", N 1)) )))
let s1 = I fac (Map.ofList [("x",4)])

// 3.4 Examples below
let ex1 = Eq(Mul(V "x" , N 10), N 10)
let ev1 = B ex1 (Map.ofList[("x",10)])

let ex2 = Mul(N 10, N 10) 
let ev2 = A ex2 (Map.empty) 

let ex3 = Mul(Add(N 10, N 20), N 10) 
let ev3 = A ex3 (Map.empty)

let ex4 = ITE(Eq((V "x"), N 0), Ass("y", N 10) , Ass("y", N 100))
let ev4 = I ex4 (Map.ofList[("x", 10)])

let ex5 = While(Eq(V "x", N 0), Seq(Ass("x",Sub(N 1,V"x")), Ass("y",Mul(V "y",N 10))))


let ev5 = I ex5 (Map.ofList[("x",4);("y",10)])

//3.6
(*
    If our state changed from each calculation it would be possible to do this by adding something along the lines of
    what is written below:
    type stm = (* statements *)
    ...
    | Inc of string 

    let rec I stm state =
    ...
    | Inc(a) -> A (V(a, I (Ass(a, (Add(V(a),N(1))) ))) state) state

    However, this piece of code would give the error
        This expression was expected to have type
            Map<string,int>    
            but here has type
            int  
    
    The reason for this is that in order to overwrite a given X with the newly calculated value it should be possible for 
    the functions that return a state also to return an integer and a Map. 
*)

//3.7   6.2

type Fexpr = | Const of float
             | X
             | Add of Fexpr * Fexpr
             | Sub of Fexpr * Fexpr
             | Mul of Fexpr * Fexpr
             | Div of Fexpr * Fexpr
             | Sin of Fexpr
             | Cos of Fexpr
             | Log of Fexpr
             | Exp of Fexpr

let rec postFix expr = 
    match expr with
    | Const(x)  -> x.ToString() + " "
    | X         -> "x "
    | Add(x, y) -> postFix x + postFix y + "+ "
    | Sub(x, y) -> postFix x + postFix y + "- "
    | Mul(x, y) -> postFix x + postFix y + "* "
    | Div(x, y) -> postFix x + postFix y + "/ "
    | Sin(x)    -> postFix x + "Sin "
    | Cos(x)    -> postFix x + "Cos "
    | Log(x)    -> postFix x + "Log "
    | Exp(x)    -> postFix x + "Exp "
    | _ -> ""
    

//3.8  6.8

(* Opgave 1 *)
type Instruction = | ADD | SUB | MULT | DIV | SIN | COS | LOG | EXP | PUSH of float

type Stack = float list

let push s x =
    x::s

let intpInstr s i =
    match i,s with
    | ADD, x::y::xs -> push s (x+y)
    | SUB, x::y::xs -> push s (x-y)
    | MULT, x::y::xs -> push s (x*y)
    | DIV, x::y::xs -> push s (x/y)
    | SIN, x::xs -> push s (sin(x))
    | COS, x::xs -> push s (cos(x))
    | LOG, x::xs -> push s (log(x))
    | EXP, x::xs -> push s (exp(x))
    | PUSH(x), s -> push s x
    | _ -> failwith "not a valid instruction for this function!"


(* 2 *)
let intpProg il =
   let rec execute instr (s:Stack) = 
        match instr with 
        | [] -> s.Head
        | x::instr -> execute instr (intpInstr s x)
   
   execute il [] 


(* 3 *)


type Fexpr = | Const of float
             | X
             | Add of Fexpr * Fexpr
             | Sub of Fexpr * Fexpr
             | Mul of Fexpr * Fexpr
             | Div of Fexpr * Fexpr
             | Sin of Fexpr
             | Cos of Fexpr
             | Log of Fexpr
             | Exp of Fexpr

let rec postFix expr = 
    match expr with
    | Const(x)  -> x.ToString() + " "
    | X         -> "x "
    | Add(x, y) -> postFix x + postFix y + "+ "
    | Sub(x, y) -> postFix x + postFix y + "- "
    | Mul(x, y) -> postFix x + postFix y + "* "
    | Div(x, y) -> postFix x + postFix y + "/ "
    | Sin(x)    -> postFix x + "Sin "
    | Cos(x)    -> postFix x + "Cos "
    | Log(x)    -> postFix x + "Log "
    | Exp(x)    -> postFix x + "Exp "
    | _ -> ""



let rec trans e v = 
    match e with
    | Const(x)  -> [PUSH x]
    | X         -> [PUSH v]
    | Add(x, y) -> (trans x v)@(trans y v)@[ADD]
    | Sub(x, y) -> (trans x v)@(trans y v)@[SUB]
    | Mul(x,y)  -> (trans x v)@(trans y v)@[MULT]
    | Div(x, y) -> (trans x v)@(trans y v)@[DIV]
    

//3.9  7.2
(*We made the dll. If you provide the correct path to the ComplexNumbers.dll it works. See subfolder*)





