// Experiment, prime factors in parallel, like fsharp/parallel2013.fs
// sestoft@itu.dk * 2013-04-24

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Diagnostics;	// Stopwatch

class MyTest {
  private static int[] histogram = new int[200000];

  public static void Main(String[] args) {
    {
      Timer t = new Timer();
      List<int>[] factors200000 = new List<int>[200000];
      for (int n=0; n<200000; n++) 
	factors200000[n] = Factors(n);
      PrintHistogram();
      Console.WriteLine("{0:F3} sec", t.Check());
    }
    histogram = new int[200000];
    {
      Timer t = new Timer();
      List<int>[] factors200000 = new List<int>[200000];
      Parallel.For(0, 200000, 
		   n => { factors200000[n] = Factors(n); });
      PrintHistogram();
      Console.WriteLine("{0:F3} sec", t.Check());
    }
  }

  static void PrintHistogram() {
    for (int i=0; i<40; i++)
      Console.Write("{0}; ", histogram[i]);
    Console.WriteLine();
  }

  static List<int> Factors(int n) {
    List<int> factors = new List<int>();
    int d = 2;
    while (n > 1) {
      if (n % d == 0) {
	factors.Add(d);
	lock (histogram)
	  histogram[d]++;
	n /= d;
      } else
	d++;      
    }
    return factors;
  }
}

public class Timer {
  private Stopwatch stopwatch;

  public Timer() {
    stopwatch = new Stopwatch();
    stopwatch.Reset();
    stopwatch.Start();
  }
  
  public double Check() {
    return stopwatch.ElapsedMilliseconds / 1000.0;
  }
}
