﻿module examProject

//Name: Jonas Kastberg Hinrichsen
//CPR: 0907923649
//Fagkode: BFNP
//Kursusnavn: Andetårsprojekt: Funktionel Programmering
//Eksamensdato: 11-06-2014
//Eksamensnummer: 11120
//Declaration: I hereby declare that I myself have created this exam hand-in in its entirety without help from anybody else.


//-------------------------- Question 1 -------------------------

type OrderedList <'a when 'a : equality> = { front: 'a list;
                                             rear: 'a list}
// Question 1.1

// a)
let ol2 = {front = ["Hans"]; rear = ["Gudrun"; "Brian"]}
let ol3 = {front = ["Hans"; "Brian"]; rear = ["Gudrun"]}

// b)
// There are four in total
// front: Hans Brian Gudrun |    rear: []
// front: Hans Brian        |    rear: Gudrun
// front: Hans              |    rear: Gudrun, Brian
// front: []                |    rear: Gudrun, Brian, Hans

// Question 1.2

// a)
// I append the reverse of the rear list to the front list, using the list append operator @
let canonical {front = f; rear = r} = {front = f@List.rev r; rear=[]}

// b)
// Return the front list of the canonical version of the input list.
// The canonical list will contain all the ordered list elements in the correct order.
let toList ol = (canonical ol).front

// Question 1.3

// a)
// Return a newly constructed ordered list with empty lists as aguments
let newOL () = {front = []; rear = []}

// b)
// By using the boolean operators '=' and '&&' I determine if both lists are empty, and return the boolean result (either true or false)
let isEmpty {front = f; rear = r} = (f = [] && r = [])

// Question 1.4

// a)
// Using the cons operator I add the element to the front of the front list
// Cons denotes the structure of a list with the head being the lefthand side of the operator and the tail list being on the righthand side
// Since there are no stated rules for whether we need to add the element to the rear list, whenever the front list is empty,
// I have chosen to always append the new result to the front of the front list.
let addFront x {front = f; rear = r} = {front = x::f; rear = r}

// b)
// Using pattern matching it is possible to check for the error case where we cant remove an element, due to an empty list.
// By pattern matching the remaining cases we can remove the element from the wanted list.
// e.g. we always remove from the front, but if the front is empty we remove from the back.
// When we process removal from the rear, we parse the reversed list to the remove first (since the rear is in reverse order)
// We also reverse the result of this function, since we need to maintain the invariance.
let removeFront ol= 
            let rec removeFirst = function
                    | [] -> failwith ("Cannot remove from empty list")
                    | l::ls -> (l, ls)

            let removeFrontAux = function
                    | {front = []; rear = []} -> failwith ("Cannot remove from empty list")
                    | {front = f; rear = []} -> let (first, list) = removeFirst f
                                                (first, {front = list; rear = []})
                    | {front = []; rear = r} -> let (first, list) = removeFirst (List.rev(r))
                                                (first, {front = []; rear = List.rev list})
                    | {front = f; rear = r} ->  let (first, list) = removeFirst f
                                                (first, {front = list; rear = r})
            removeFrontAux ol

// c)
// Return the first element in the removeFront function, since this returns the peeked value.
// The removeFront function inherently handles errors such as empty lists
let peekFront os = fst (removeFront os)

// Question 1.5
// Since there are no stated rules for the appending, other than the two lists needs to be appended to a complete ordered list
// which satisfies the invariant, i have chosen to split them up, such that each lists end up in the front and rear list of the final ordered list
// This means that the appending of list f1=[e1;e2] r1=[e4;e3] and list f2=[i1;i2] r2=[i4;i3] results in f3=[e1;e2;e3;e4] r3 = [i4;i3;i2;i1]
let append ol1 ol2 = {front =  (toList ol1); rear = List.rev (toList ol2)}

// Question 1.6

// Using the list.map function we apply the given function to each of the elements in the front and rear lists individually
let map func {front = f; rear = r} = {front = List.map func f; rear = List.map func r}

// Question 1.7

// By using the list.fold function we fold over the ordered list once converted to a single list, to obtain the final result.
// Fold applies the given function to the current state and the next given element.
let fold s init ol = List.fold s init (toList ol)

// Question 1.8

// By using the fold function from 1.7, we simply define the function to be folded, as well as the initial state being the empty map.
// The map will then be added an element during each fold, using an if then else statement to check whether to add the element, or update the multiplicity.
let multiplicity ol = fold (fun (s:Map<'a, int>) e -> if(s.ContainsKey(e)) then (s.Add(e, (s.Item e)+1)) else s.Add(e, 1)) (Map.ofList []) ol

//-------------------------- Question 2 -------------------------

// Question 2.1

// a)
//The function f returns a list where i has been added to each of the values. i is being incremented in each recursive step.
//In the last call i is appended to the list. The list [e1; e2; e3; e4] will thus become [e1+i; e2+(i+1); e3+(i+2); e4+(i+3); (i+4)]

// b)
//The base case '[]' returns a new list with i as the only element. This means that no call can be made to f which returns the empty list.

// c)
//The base case is met depending on the input list. In each of the recursive calls the list size is reduced by one. 
//Since a list cannot be infinite (since it is not a sequence) nor of negative size, it is not possible to reach an infinite loop using any input

// Question 2.2

// The acculumating parameter a accumulates the results through each of the recursive calls.
// Since each recursive result is appended towards the end of the list, we use the @ (append) list operator to append the new element to a.
// The use of the @ operator is costly, and a more delicate method might have been introduced by other means.
// However the purpose of this assignment was to use an accumulator to reach tail recursion,
// so no further effort has been made towards improving the runtime result.
let fA i xs = 
        let rec fAAux i a = function
                            | [] -> a @ [i]
                            | x::xs -> fAAux (i+1) (a @ [(i+x)]) xs
        fAAux i [] xs

// Question 2.3

// The continuation c is a function that lets us avoid non-tail recursion.
// A call to c at any stage will return what we have "continued" so far. e.g. The input to c should therefore be the "rest" of the computation,
// in this case denoted as "res". Through each recursive step, we develop the coninuation so that when reaching the base case
// The continued computations will be the result, except the base case feeded value.
// Thus by feeding the "rest" of the computation (in this case being [i] we receive the total result since the continuation contains the computations
// up until this step.
// Thus, when we reach the base case, we feed [i] to the continuation, which then performs the entire calculation
// We use an auxillary function to prevent the user from feeding wrong base inputs (c needs to be the id function) to the method initially.
let fC i xs =
        let rec fCAux i c = function
                            | [] -> c [i]
                            | x::xs -> fCAux (i+1) (fun res -> c(x+i::res)) xs
        fCAux i id xs
        
//-------------------------- Question 3 -------------------------

// Question 3.1

// A call to myFinSeq creates a sequence with the length M, that consists of numbers, starting from n, and then incremented by n for each step.
// The sequence is inclusive meaning that m is also included as input for the last element, rather than it being (M-1)
// The sequence retreived could as an example describe a 7-table by giving it n=7 and m=10
// seq[7;14;21;28;35;42;49;56;63;70]

// Question 3.2

// By using initInfinite we declare the function that each of the elements should be defined from. i is the index of the element.
// The sequence is infinite and starts from 0.
let mySeq n = Seq.initInfinite (fun i -> n+n*i)

// Question 3.3

// The question defines that the range should be from 0 to N and 0 to M, however in assignment 3.4 it is noted that the first 4 elementes of the function
// is (1,1,1) (1,2,2) (1,3,3) and (1,4,4) denoting that the iteration starts from 1. Therefore i have chosen to implement it as such.

// By using Seq.collect we collect all the results yielded from a sequence expression. By using a nested seq.collect, it is possible to make a
// "for-loop" as known in objective programming, thus retrieving the results for all combinations of n and m.
// This could have been done in a prettier fashion using more exotic sequential expressions (computation expressions).
// Negative numbers are not handled for the function, but since the list builder simply creates an empty set in the case,
// I have chosen not to handle the error myself.
let multTable N M = Seq.collect (fun n -> Seq.collect (fun m -> seq{yield (n,m,n*m)}) [1..M]) [1..N]

// Question 3.4

// Seq.map iterates through all elements of the given sequences (retrieved from multTable), and maps them to a new table.
// I use this to convert each of the tuples to a string representative, which is then mapped to the same index in the new sequence
let ppMultTable N M = Seq.map (fun (x,y,z) -> ""+x.ToString()+" * "+y.ToString()+" is "+z.ToString()) (multTable N M)

//-------------------------- Question 4 -------------------------

// Definition of the types as described in the assignment
type opr = | MovePenUp
           | MovePenDown
           | TurnEast
           | TurnWest
           | TurnNorth
           | TurnSouth
           | Step

type plot = | Opr of opr
            | Seq of plot * plot

// Question 4.1

// a)
// We simply return a string accomodated with the pattern matched tagged value.
let rec ppOpr = function
                | MovePenUp -> "MovePenUp"
                | MovePenDown -> "MovePenDown"
                | TurnEast -> "TurnEast"
                | TurnWest -> "TurnWest"
                | TurnNorth -> "TurnNorth"
                | TurnSouth -> "TurnSouth"
                | Step -> "Step"

// b)
// We pattern match the input plot. 
// If the plot is an operation we use the ppOpr function to convert it to a string.
// If the plot is a sequence we append the recursive calls of both plots in the sequence using the " => " string.
// The recursive call will end with the base case operations being translated to strings with " => " in between them.
let rec ppOprPlot = function
                    | Opr(op) -> ppOpr op 
                    | Seq(se1, se2)-> ppOprPlot se1 + " => " + ppOprPlot se2

// Question 4.2

// Definition of the types as described in the assignment
type dir =  | North
            | South
            | West
            | East

type pen =  | PenUp
            | PenDown

type coord = int * int

type state = coord * dir * pen

let initialState = ((0,0), East, PenUp)

// a)
// We pattern match the direction of the input state to determine how to compute the result.
// The result is the resulting state where the coordinates has been modified based on the direction of the mouse.
let goStep (s:state) = match s with
            | ((cdx, cdy), East, p) -> ((cdx+1, cdy), East, p)
            | ((cdx, cdy), West, p) -> ((cdx-1, cdy), West, p)
            | ((cdx, cdy), North, p) -> ((cdx, cdy+1), North, p)
            | ((cdx, cdy), South, p) -> ((cdx, cdy-1), South, p)

// b)
// We add a dot based on the input operation.
// By pattern matching each operation case, we can define how to compute each case.
// Whenever we get a direction we simply modify the state of the mouse to accomodate the input.
// Whenever we receive operations that adds dots to the list, we cons them on to the input coord list.
// For ease of use, I use the goStep function to modify the state whenever a Step operation is received.
// We add the newly acquired coordinate thereafter.
let addDot (cd, d, p) cs = function
                    | MovePenUp -> (cs, (cd, d, PenUp))
                    | MovePenDown -> (cd::cs, (cd, d, PenDown))
                    | TurnEast -> (cs, (cd, East, p))
                    | TurnWest -> (cs, (cd, West, p))
                    | TurnNorth -> (cs, (cd, North, p))
                    | TurnSouth -> (cs, (cd, South, p))
                    | Step -> let (ncd, d, p) = goStep(cd, d, p) in
                                    (ncd::cs, (ncd, d, p))

// c)
// To dot the coords of a plot, we use the base case plot operation Opr to execute the addDot function. The result is the new state and coordinate list.
// When we pattern match Seq, we make sure to first compute the left branch, receiving the resulting coordinate list / state.
// We then feed this information into the right branch. In the end the recursive call will reach the base case Operation, 
// while not needing to feed the result into more sequence operations, thus yielding the result.
let dotCoords p= 
            let rec dotCoordsAux s cs = function
                                | Opr(o) -> (addDot s cs o)
                                | Seq(s1, s2) -> let (ncs, ns) = dotCoordsAux s cs s1 in
                                                 dotCoordsAux ns ncs s2
            fst(dotCoordsAux initialState [] p)

// d)
// By using the set function ofList we can simply convert the dot list to a set. By definition the set will discard any duplicate added values.
let uniqueDotCoords p = Set.ofList (dotCoords p)

// Question 4.3

// To avoid unneccesary overriding of the + operator, I have chosen to use the .+ operator instead of the regular +.
// By using the special annotation we have declared that the function is an infix operator.
// This means that the function will take the first argument from the left hand side and the second from the right hand side.
// We simply return the Seq construct by feeding it with the two arguments.
let (.+) p1 p2 = Seq(p1, p2)