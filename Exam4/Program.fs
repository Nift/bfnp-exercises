﻿

type OrderedList<'a when 'a : equality> = { front: 'a list;
                                            rear: 'a list}

let ex = {front = ['x']; rear=['z';'y']}

exception Error of string


//Question 1.1

let ol1 = {front = ["Hans"; "Brian"; "Gudrun"]; rear =[]}
let ol2 = {front = ["Hans"; "test"]; rear=["Gudrun"; "Brian"]}
let ol3 = {front = ["Hans"; "Brian"]; rear = ["Gudrun"]}
let ol4 = {front = []; rear = ["Hans"; "Brian";"Gudrun"]}

let invariant o = 
    match o with
    | {front = x; rear = y} -> x @ List.rev y



//Question 1.2

let canonical ol = 
    match ol with
    | {front = x; rear = y} -> { front = x @ List.rev y; rear = []}

let toList ol = (canonical ol).front
    

//Question 1.3

let newOL = {front = []; rear = []}

let isEmpty ol = if ol.front = [] && ol.rear = [] then true else false


//Question 1.4
let addFront (v : 'a) (ol : OrderedList<'a>) = { front = v::ol.front; rear = ol.rear}

let removeFront (ol:OrderedList<'a>) = 
    match ol.front with
    | x::xs -> (x, {front = xs; rear = ol.rear})
    | [] -> raise (Error "empty list")

let peekFront (ol:OrderedList<'a>) = (toList ol).Head

//Question 1.5

let append (ol1:OrderedList<'a>) (ol2:OrderedList<'a>) = {front = toList ol1; rear = List.rev (toList ol2)}

//Question 1.6
let map f (ol:OrderedList<'a>) = {front = List.map f ol.front; rear = List.map f ol.rear}

//Question 1.7
let fold f v (ol:OrderedList<'a>) = List.fold f v (toList ol) 

//Question 1.8
let multiplicity (ol:OrderedList<'a>) = fold (fun acc e -> if Map.containsKey e acc then Map.add e (Map.find e acc + 1) acc else Map.add e 1 acc) Map.empty ol


//Question 2

let rec f i = function 
      [] -> [i]
    | x::xs -> i+x :: f (i+1) xs


//Question 2.1 
 //The function computes an int list, where i has been added to every element of the list. Additionally, it should be noted that i is incremented by 1 for every iteration, meaning that the first element of the list will be that element plus i and the second element would be the second element + i +1 
   
//The result can never be an empty list, as if an empty list is given the result of the function will be simply be the i originally specified contained in a list. 

//Technically it is possible to make the function run in an infinite loop if the list given is infinite, for instance in the case below:
//let result = f 1 (Seq.toList (Seq.initInfinite (fun i -> if i % 2 = 0 then -i else i)))
//How it can be argued that this is not the function creating an infinite loop, but the Seq.toList function. Therefore it is in most cases not possible for the function to run infinitely. 
//Additionally, it should be noted that F#'s list does not lazy load, therefore making it impossible to create an infinite list. 

//Question 2.2

let rec fA i l acc = 
    match l with
    | [] -> acc@[i]
    | x::xs -> fA (i+1) xs (acc@[i+x])


//Question 2.3
let rec fC i l c = 
    match l with
    | [] -> c [i]
    | x::xs -> fC (i+1) xs (fun res -> (c [(x+i)]) @ res)



//Question 3

//Question 3.1
let myFinSeq n M = Seq.map (fun m -> n+n*m) [0..M]

//This function creates a sequence where every element from 0 to the given M, is the given value n is added to the multiplication of n and the the given element in the list. 
//In other words, then the map functions iterates over every element in the list, and updates it to be the given element times n plus n. 

//Quesiton 3.2

let mySeq n = Seq.initInfinite (fun i -> if i >= 0 then n+n*i else i)


//Question 3.3 

let multTable N M = Seq.collect(fun n -> Seq.collect (fun m -> seq{ yield (n,m,n*m)}) [0..N]) [0..M]
    
//Question 3.4

let ppMultTable N M = 
    Seq.collect (fun (x,y,z) -> x.ToString() + " * " + y.ToString() + " is " + z.ToString()) (multTable N M)


//Question 4

type opr =
        | MovePenUp
        | MovePenDown
        | TurnEast
        | TurnWest
        | TurnNorth
        | TurnSouth
        | Step

type plot = Opr of opr
            | Seq of plot * plot
             with static member (+) ((p1 :plot), (p2:plot)) : plot = Seq(p1,p2)
            

let side = Seq(Opr MovePenDown, Seq(Opr Step, Seq(Opr Step, Opr Step)))
let rect = Seq(Seq(Opr TurnEast, side), Seq(Opr TurnNorth, Seq(side, Seq(Opr TurnWest, Seq(side, Seq(Opr TurnSouth, side))))))


//Question 4.1
let ppOpr opr = 
    match opr with
    | MovePenUp -> "MovePenUp"
    | MovePenDown -> "MovePenDown"
    | TurnEast -> "TurnEast"
    | TurnWest -> "TurnWest"
    | TurnNorth -> "TurnNorth"
    | TurnSouth -> "TurnSouth"
    | Step -> "Step"

let ppOprPlot p = 
    let rec iterate s acc = 
        match s with
        | Seq(o, s) -> (iterate s (iterate o acc)) 
        | Opr s -> acc + " => " + ppOpr s
    let s = iterate p ""
    s.Substring(4,s.Length-4)


//Question 4.2

type dir = 
          | North
          | South
          | West
          | East
type pen = 
          | PenUp
          | PenDown

type coord = int * int
type state = coord * dir * pen

let initialState = ((0,0), East, PenUp)

let goStep s =
    match s with
    | ((x,y), dir, pen) -> match dir with
                            | East -> ((x+1, y), dir, pen)
                            | West -> ((x-1, y), dir, pen)
                            | North -> ((x, y+1), dir, pen)
                            | South -> ((x, y-1), dir, pen)
                            

let addDot (s:state) coords opr = 
    match s with
    | ((x,y), dir, pen) -> match opr with 
                            | MovePenDown -> ((x,y)::coords, ((x,y), dir, PenDown))
                            | MovePenUp -> (coords, ((x,y), dir, PenUp))
                            | TurnEast -> (coords, ((x,y), East, pen))
                            | TurnWest -> (coords, ((x,y), West, pen))
                            | TurnNorth -> (coords, ((x,y), North, pen))
                            | TurnSouth -> ( coords, ((x,y), South, pen))
                            | Step -> match goStep s with
                                      | ((x,y), dir, pen) -> ( (x,y)::coords, ((x,y),dir,pen))
                            

let dotCoords s = 
    let rec iterate p coords state =
        match p with
        | Seq(s,t) -> match iterate s coords state with
                      | (c,s) -> iterate t c s
        | Opr s -> addDot state coords s
   
    match iterate s [] initialState with
    | (coords, state) -> coords


let uniqueDotCoords s : Set<coord>=  List.fold (fun acc e -> Set.add e acc) Set.empty (dotCoords s)


//Quesiton 4.3

//type plot with static member (+) ((p1 :plot), (p2:plot)) = Seq(p1,p2)

        //    | (+) : plot2*plot2 -> plot2

let side2 = Opr MovePenDown + Opr Step + Opr Step + Opr Step
let rect2 = Opr TurnEast + side2 + Opr TurnNorth + side2 + Opr TurnWest + side2 + Opr TurnSouth + side2
