﻿// 2013-04-18 * sestoft@itu.dk

// Similar to the development of monad-based interpreters from
// scala/ExpressionsMonads.scala

// Very simple expressions

type expr =
    | CstI of int
    | Prim of string * expr * expr
    | Prim1 of string * expr
    | PrimSum of string * expr list


// ------------------------------------------------------------

// Plain evaluator, return type int

let rec eval1 e : int =
    match e with
    | CstI i -> i
    | Prim(op, e1, e2) ->
        let v1 = eval1 e1
        let v2 = eval1 e2
        match op with
        | "+" -> v1 + v2
        | "*" -> v1 * v2
        | "/" -> v1 / v2
    //Exercise 1 here
    | Prim1(op, e1) ->
        let v1 = eval1 e1
        if v1 < 0 then v1 * -1 else v1
    //Exercise 2 here
    | PrimSum (op, e1) -> 
        let es = List.map (eval1) e1
        match op with 
        | "+" -> 
            let rec sum l =
                match l with
                | x::xs -> x + sum xs
                | [] -> 0
            sum es
        | "*" -> 
            let rec sum l = 
                match l with
                | x::xs -> x * sum xs
                | [] -> 1
            sum es
        | "/" ->  //This one is slightly weird, however we included it for continuity.
            let rec sum l =
                match l with
                | x::xs -> x / sum xs
                | [] -> 1
            sum es

let opEval op v1 v2 : int =
    match op with
    | "+" -> v1 + v2
    | "*" -> v1 * v2
    | "/" -> v1 / v2
    //Exercise 1
    | "ABS" -> if v1 < 0 then v1 * -1 else v1
    
    
//This solution adds the match on op back into the function.
//However, as no operation is done it is deemed a suitable solution, in order to achieve a more
//generalised sum function.
//This function was initially inside of eval2, however to utilize it easily in eval3 it has been moved outside of 
//the copse of eval2. 
let sum op l= 
    let acc = 
            match op with
            | "+" -> 0
            | "*" -> 1 
            | "/" -> 1
            | _ -> 0

    let rec whatSum op l a = 
        match l with
        | x::y::xs -> whatSum op xs (opEval op a (opEval op x y))
        | x::xs -> opEval op a x
        | [] -> a

    whatSum op l acc

let rec eval2 e : int =
    match e with
    | CstI i -> i
    | Prim(op, e1, e2) ->
        let v1 = eval2 e1
        let v2 = eval2 e2
        opEval op v1 v2
     //Exercise 1
    |Prim1(op, e1) -> 
        let v1 = eval2 e1
        opEval op v1 0
    //Exercise 2
    |PrimSum(op, eList) -> 
        sum op (List.map (eval2) eList)




type IdentityBuilder() =
    member this.Bind(x, f) = f x
    member this.Return x = x
    member this.ReturnFrom x = x

let identM = new IdentityBuilder();;

let rec eval3 e : int =
    match e with
    | CstI i -> identM { return i }
    | Prim(op, e1, e2) ->
        identM  { let! v1 = eval3 e1
                  let! v2 = eval3 e2
                  return! opEval op v1 v2 }
    //Exercise 1
    | Prim1(op, e1) ->
        identM { let! v1 = eval3 e1
                return! opEval op v1 0}
    //Exercise 2
    | PrimSum(op, eList) ->
        identM { return! sum op (List.map (eval3) eList) }

// ------------------------------------------------------------

// Evaluator that may fail, return type: int option

let rec optionEval1 e : int option =
    match e with
    | CstI i -> Some i
    | Prim(op, e1, e2) ->
        match optionEval1 e1 with
        | None -> None
        | Some v1 ->
            match optionEval1 e2 with
            | None -> None
            | Some v2 ->
                match op with
                | "+" -> Some(v1 + v2)
                | "*" -> Some(v1 * v2)
                | "/" -> if v2 = 0 then None else Some(v1 / v2)
    //Exercise 1
    | Prim1(op, e1) ->
        match optionEval1 e1 with
            | None -> None
            | Some v1 ->
                match op with
                | "ABS" -> if v1 < 0 then Some (v1 * -1) else Some v1
    | PrimSum(op, eList) ->
        let es = List.map (optionEval1) eList
        //The reason why a function very similar to  optionSum defined below, is defined here is to stay in the spirit of "bad" code. 
        let rec sumDupper op list a =
                match list with
                | x::xs -> 
                    let p = 
                        match x with
                        | Some i -> i
                    
                    let res = match op with
                        | "+" -> (a+p)
                        | "*" -> (a*p)
                        | "/" -> (a/p)
                    sumDupper op xs res
                | [] -> a
         
         
        Some (sumDupper op es 0)
         
            

let opEvalOpt op v1 v2 : int option =
    match op with
    | "+" -> Some(v1 + v2)
    | "*" -> Some(v1 * v2)
    | "/" -> if v2 = 0 then None else Some(v1 / v2)
    //Exercise 1
    | "ABS" -> if v1 < 0 then Some (v1 * -1) else Some v1
     
//Exercise 2
//Modified sum function to deal with options
let optionSum op l = 
    let acc = 
            match op with
            | "+" ->  0
            | "*" ->  1 
            | "/" ->  1
            | _ ->  0
    
    let rec whatSum op l a = 
        match l with
        | x::y::xs -> 
            let q =
                match x with
                | Some i -> i
            let w = 
                match y with
                | Some i -> i
            

            let res =
                match opEvalOpt op q w with
                    | Some i -> i
            
            let res2 = 
                match opEvalOpt op a res with
                    | Some i -> i
            
            
            whatSum op xs res2
        | x::xs -> 
            let q = 
                match x with
                | Some i -> i
            opEvalOpt op a q
        | [] ->  Some a

    whatSum op l acc   

   
let rec optionEval2 e : int option =
    match e with
    | CstI i -> Some i
    | Prim(op, e1, e2) ->
        match optionEval2 e1 with
        | None -> None
        | Some v1 ->
            match optionEval2 e2 with
            | None -> None
            | Some v2 -> opEvalOpt op v1 v2
    //Exercise 1
    | Prim1(op, e1) ->
        match optionEval2 e1 with
        | None -> None
        | Some v1 -> 
            opEvalOpt op v1 0
    //Exercise 2
    |PrimSum(op, eList) ->
        let es = List.map (optionEval2) eList
        optionSum op es

let optionFlatMap (f : 'a -> 'b option) (x : 'a option) : 'b option =
    match x with
    | None   -> None
    | Some v -> f v;;

type OptionBuilder() =
    member this.Bind(x, f) =
        match x with
        | None   -> None
        | Some v -> f v
    member this.Return x = Some x
    member this.ReturnFrom x = x
 
let optionM = OptionBuilder();;

let rec optionEval3 e : int option =
    match e with
    | CstI i -> optionM { return i }
    | Prim(op, e1, e2) ->
        optionM { let! v1 = optionEval3 e1
                  let! v2 = optionEval3 e2
                  return! opEvalOpt op v1 v2 }
    //Exercise 1
    | Prim1(op, e1) -> 
        optionM { let! v1 = optionEval3 e1
                  return! opEvalOpt op v1 0 }
    //Exercise 2
    | PrimSum(op, eList) -> 
        optionM { return! optionSum op (List.map (optionEval3) eList) }



// ------------------------------------------------------------                

// Evaluator that returns a set of results, return type: int Set

let opEvalSet op v1 v2 : int Set =
    match op with
    | "+" -> Set [v1 + v2]
    | "*" -> Set [v1 * v2]
    | "/" -> if v2 = 0 then Set.empty else Set [v1 / v2]
    | "choose" -> Set [v1; v2]
    //Exercise 1
    | "ABS" -> if v1 < 0 then Set [(v1 * -1)] else Set [v1]

//Exercise 2
//Modified sum function to deal with sets
let setSum op l = 
    
    let acc = 
        match op with
        | "+" -> Set [0]
        | "*" -> Set [1]
        | "/" -> Set [1]
        | _   -> Set [0]

    let rec sumStuff op l a =
        match l with
        | x::y::xs -> sumStuff op xs (opEvalSet op (Set.maxElement a) (Set.maxElement (opEvalSet op (Set.maxElement x) (Set.maxElement y)) ))
        | x::xs -> opEval op (Set.maxElement a) (Set.maxElement x)
        | [] -> Set.maxElement a

    let p = sumStuff op l acc
    Set [p]
   

let rec setEval1 e : int Set =
    match e with
    | CstI i -> Set [i]
    | Prim(op, e1, e2) ->
        let s1 = setEval1 e1
        let yss = Set.map (fun v1 ->
                           let s2 = setEval1 e2
                           let xss = Set.map (fun v2 -> opEvalSet op v1 v2) s2
                           Set.unionMany xss)
                          s1
        Set.unionMany yss
    //Exercise 1
    | Prim1(op, e1) ->
        let s = setEval1 e1
        let xss = Set.map (fun v1 -> opEvalSet op v1 0) s
        Set.unionMany xss
    //Exercise 2
    | PrimSum(op, eList) -> 
        let es = List.map (setEval1) eList
        setSum op es
   
        
        
        

let setFlatMap (f : 'a -> 'b Set) (x : 'a Set) : 'b Set =
    Set.unionMany (Set.map f x);;

type SetBuilder() =
    member this.Bind(x, f) =
        Set.unionMany (Set.map f x)
    member this.Return x = Set [x]
    member this.ReturnFrom x = x
 
let setM = SetBuilder();;

let rec setEval3 e : int Set =
    match e with
    | CstI i -> setM { return i }
    | Prim(op, e1, e2) ->
        setM { let! v1 = setEval3 e1
               let! v2 = setEval3 e2
               return! opEvalSet op v1 v2 }
    //Exercise 1
    | Prim1(op, e1) -> 
        setM {  let! v1 = setEval3 e1
                return! opEvalSet op v1 0}
    //Exercise 2
    | PrimSum(op, eList) ->
        setM {
                let! v = Set [List.map (setEval3) eList]
                return! setSum op v}



// ------------------------------------------------------------

// Evaluator that records sequence of operators used,
// return type: int trace

let random = new System.Random()

type 'a trace = string list * 'a

let opEvalTrace op v1 v2 : int trace =
    match op with
    | "+" -> (["+"], v1 + v2)
    | "*" -> (["*"], v1 * v2)
    | "/" -> (["/"], v1 / v2)
    | "choose" -> (["choose"], if random.NextDouble() > 0.5 then v1 else v2)
    | "ABS" -> if v1 < 0 then (["ABS"], (v1 * -1)) else (["ABS"], v1)

//Exercise 2
//Sum function from earlier modified to use trace. 
let traceSum op l= 
    let acc = 
            match op with
            | "+" -> 0
            | "*" -> 1 
            | "/" -> 1
            | _ -> 0
    
    let rec whatSum op l (t,a) = 
        match l with
        | (trace1, x)::(trace2, y)::xs -> 
            let (trace3, res1) = opEvalTrace op x y
            let (trace4, res2) = (opEvalTrace op a res1)
            whatSum op xs (t @ trace1 @ trace2 @ trace3 @ trace4, res2)
        | (trace1, x)::xs -> 
                let (trace2, res) = opEvalTrace op a x
                (t @ trace1 @ trace2, res)
        | [] -> (t, a)

    whatSum op l ([],acc)

let rec traceEval1 e : int trace =
    match e with
    | CstI i -> ([], i)
    | Prim(op, e1, e2) ->
        let (trace1, v1) = traceEval1 e1
        let (trace2, v2) = traceEval1 e2
        let (trace3, res) = opEvalTrace op v1 v2
        (trace1 @ trace2 @ trace3, res)
    | Prim1(op, e1) -> 
        let (trace1, v1) = traceEval1 e1
        let (trace3, res) = opEvalTrace op v1 0
        (trace1 @ trace3, res)
    //Exercise 2
    | PrimSum(op, eList) -> 
        let es = List.map (traceEval1) eList
        traceSum op es

let traceFlatMap (f : 'a -> 'b trace) (x : 'a trace) : 'b trace =
    let (trace1, v) = x
    let (trace2, res) = f v
    (trace1 @ trace2, res)

type TraceBuilder() =
    member this.Bind(x, f) =
        let (trace1, v) = x
        let (trace2, res) = f v
        (trace1 @ trace2, res)
    member this.Return x = ([], x)
    member this.ReturnFrom x = x
 
let traceM = TraceBuilder();;

let rec traceEval3 e : int trace =
    match e with
    | CstI i -> traceM { return i }
    | Prim(op, e1, e2) ->
        traceM { let! v1 = traceEval3 e1
                 let! v2 = traceEval3 e2
                 return! opEvalTrace op v1 v2 }
    | Prim1(op, e1) -> 
        traceM { let! v1 = traceEval3 e1
                 return! opEvalTrace op v1 0}
    //Exercise 2
    | PrimSum(op, eList) -> 
        traceM { let! es = ([], List.map (traceEval3) eList)
                 return! traceSum op es }

// ------------------------------------------------------------

let expr1 = Prim("+", CstI(7), Prim("*", CstI(9), CstI(10)))
let expr2 = Prim("+", CstI(7), Prim("/", CstI(9), CstI(0)))
let expr3 = Prim("+", CstI(7), Prim("choose", CstI(9), CstI(10)))
let expr4 = Prim("choose", CstI(7), Prim("choose", CstI(9), CstI(13)))
let expr5 = Prim("*", expr4, Prim("choose", CstI(2), CstI(3)))



//For testing

let expr10 = Prim1("ABS", Prim("+", CstI(7), Prim("*", CstI(-9), CstI(10))))
let expr11 = Prim1("ABS", Prim("+", CstI(7), Prim("/", CstI(9), CstI(1))))
let expr12 = Prim("+", CstI(7), Prim("choose", Prim1("ABS", CstI(-9)), CstI(10) ))
let expr13 = PrimSum("+", [CstI(7); CstI(8); CstI(1)])

let expr14 = PrimSum("+", [CstI(1);CstI(1);CstI(1);CstI(1)])
let expr15 = PrimSum("*", [CstI(2);CstI(3);CstI(1);CstI(4)])
let expr16 = PrimSum("/", [CstI(100);CstI(1);CstI(7);CstI(4)])
