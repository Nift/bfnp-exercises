﻿module Exercise5


(* Problem 1 *)

                  
let test1 = [("a",20);("a",10);("c",35);("b",20)]
let test = [("a",1);("b",53);("c",2);("a",1);("s",90);("s",102)]

type Name = string
type Score = int
type Result = Name * Score


//1. 
let rec legalResults xs = 
    match xs with
    | [] -> true
    | (n,s)::xs -> if s > -1 && s < 101 then legalResults xs else false

legalResults test
legalResults test1

//2.
let rec maxScore (xs:Result list) = 
   match xs with
   | [] -> 0
   | (x, n)::xs -> if (n > maxScore xs) then n else maxScore xs
 
maxScore test

//3. 
let rec bestResult (xs:Result list) =
    match xs with
    | [] -> ("",0)
    | (n,s)::(n1,s1)::[] -> if (s<s1) then (n1,s1) else (n,s)
    | (n,s)::(n1,s1)::xs -> if (s<s1) then bestResult ((n1,s1)::xs) else bestResult ((n,s)::xs)

bestResult test
   
//4.
let  AvgRes (xs:Result list) = float (List.fold(fun acc (n,s) -> s+acc) 0 xs)/float (xs.Length)

AvgRes test

//5.
let rec delete (r:Result) xs  = 
    match xs with
    |[] -> xs
    |x::xs when r=x -> xs
    |x::xs -> x::delete r xs                      

delete ("a",10) test1


//6
let res:Result list = []
let rec bestN (rs:Result list) n =
    match n with
    |0 -> res
    |n when n>rs.Length -> failwith "list is too damn short"
    |n -> let y = bestResult rs
          y:: bestN (delete y rs) (n-1)
                  
bestN test1 2


(* Problem 2 *)

type Typ = 
    | Integer
    | Boolean
    | Ft of Typ list * Typ

type Decl = string * Typ

// 1

let rec distinctVars dList = 
    match dList with
    | [] -> true
    | (s,t)::dList -> if List.exists (fun (s1, t1) -> s = s1) dList then false else distinctVars dList

let test2 = [("x", Integer); ("y", Boolean)]


let assign1Test = distinctVars test2

// 2.
type SymbolTable = Map<string, Typ>

let toSymbolTable (dList:Decl list) = 
    let rec ST list (m:SymbolTable) =
        match list with
        | [] -> m
        | (s,t)::list -> ST list (m.Add(s, t))
    ST dList Map.empty


//3. 
let rec extendST (table:SymbolTable) (dList:Decl list) = 
    match dList with
    | [] -> table
    | (s,t)::dList -> extendST (table.Add(s,t)) dList

let test3 = toSymbolTable([("z", Integer); ("x", Boolean)])

extendST test3 test2

//4.

type Exp = | V of string
           | A of string * Exp list
            
let rec symbolsDefined (sym:SymbolTable) (e:Exp) =
    match e with
    | V(s) -> Map.containsKey s sym
    | A(s, e) -> if Map.containsKey s sym then List.forall (symbolsDefined sym) e else false
    

let testE = A("+", [V("z"); V("y")] )

let testSym = toSymbolTable([("z", Boolean); ("+", Ft( [Integer; Integer], Integer)); ("y", Integer)])

//5. 

let rec typOf (sym:SymbolTable) (e:Exp) =
    match e with
    | V(s) -> Map.find s sym
    | A(s, eList) -> 
        match Map.find s sym with
        | Ft(iList, bVar) -> if List.map (typOf sym) eList = iList then bVar else failwith "Not well-typed"


//6. 

type Stm = | Ass of string * Exp        //Assignment
           | Seq of Stm * Stm           //Sequential composition
           | Ite of Exp * Stm * Stm     //if-then-else
           | While of Exp * Stm         //while
           | Block of Decl list * Stm   //block

let rec wellTyped (sym:SymbolTable) (s:Stm) = 
    match s with
    | Ass(x, e) -> typOf sym (V x) = typOf sym e
    | Seq(s1, s2) -> wellTyped sym s1 && wellTyped sym s2
    | Ite(e, s1, s2) -> symbolsDefined sym e && typOf sym e = Boolean && wellTyped sym s1 && wellTyped sym s2
    | While(e, s1) -> symbolsDefined sym e && typOf sym e = Boolean && wellTyped sym s1
    | Block (dList, s1) -> if distinctVars dList then wellTyped (extendST sym dList) s1 else false

(* Made for easier testing*)
let testStm = Ass("x", V("y"))
let testerSym = toSymbolTable( [("x", Integer);("+", Ft( [Integer; Integer], Integer)); ("y", Boolean)])
let testStm2 = Seq(Ass("x", V("x")), Ass("y", V("y") ))
let testStmt3 = Ite(A(">", [V("x"); V("y")] ), Ass("x", V("x")), Ass("y", V("y") ) )
let testerSym2 = toSymbolTable( [("x", Integer);(">", Ft( [Integer; Integer], Boolean)); ("y", Integer)])



(* Problem 3*)
type T<'a,'b> = |A of 'a
                |B of 'b
                |C of T<'a,'b> * T<'a,'b>     


let rec h a b =
    match a with
    |[] -> b
    |c::d -> c::(h d b)


let rec f1 = function
    |C(t1,t2) -> 1 + max (f1 t1)(f1 t2)
    | _       -> 1

let rec f2 = function
    |A e | B e -> [e]
    |C(t1,t2)  -> f2 t1 @ f2 t2


let rec f3 e b t =
    match t with
    |C(t1,t2) when b -> C(f3 e b t1, t2)
    |C(t1,t2)        -> C(t1, f3 e b t2)
    |_        when b -> C(A e, t)
    |_               -> C(t, B e)


(* 1
The function h takes 2 arguments. A list of 'a elements and another of 'a elements.
It builds the first list again untill this list is empty, after which it concatenates
the seccond list to the end of the first(new) list. 

Its type is 'a list -> 'a list -> 'a list
*)

// 2
let ass2 = C(T.A 42, T.B true)
// val ass2 : T<int,bool> = C (A 42,B true)

//3
let ass3 = C(T.A [], T.B None)
// val ass3 : T<'a list,'b option>

//4
(*
1. 
Will compute the number of arguments
val f1 : _arg1:T<'a,'b> -> int

2.
Will create a list of 'a arguments given to the function
val f2 : _arg1:T<'a,'a> -> 'a list

3. 
This function takes a generic value 'a, a boolean and a an argument of our generic type.
As to the purpose... Depending on true and false it switches the arguments. 
When false the first argument 'a is made last as T.B.
When true the first argument 'a is put in front as T.A while t remains T.A
Yes.
val f3 : e:'a -> b:bool -> t:T<'a,'a> -> T<'a,'a>
*)


